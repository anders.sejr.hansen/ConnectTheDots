% ConnectTheDots.m
% Written by Anders Sejr Hansen, latest update, April 2018
clc; clearvars -except FrameData MetaData; %close all;

% --- DESCRIPTION --- Version 0.1
% Please see the ReadMe file on GitLab: https://gitlab.com/anders.sejr.hansen/ConnectTheDots

% This code stores key information in a semi-object oriented manner:
%   1. Global information (time-independent) is stored in "MetaData"
%   2. Frame-by-Frame info is stored in "FrameData"

% There are multiple steps to the analysis. Define what to do
CurrStep = 2;
%  CurrStep = 1: only analyze first and last frame to adjust thresholds
%  CurrStep = 2: threshold is good, test tracking parameters
Step6Plots = 1; Step7Plots = 0; % will only do plots if == 1. 

% All of the serious operations are performed using sub-functions which are
% called from the directory "Functions" and its sub-directories. 
addpath(genpath('Functions'));
% This main script is meant to serve both as a general interface and as a
% "lab notebook", but having a list of all the previous experiments and the
% parameters used to analyse them as part of this script. Toggle between
% DataSets by changing the DataSet variable. Use c1 for the first color and
% c2 for the second color. 

% What to Analyse:
DataSet = 2;

% General microscope data (all in units of nanometers/minutes where possible):
%if ~exist('MetaData','var'); MetaData = struct('PixelSize', 64, 'zStep', 500, 'RefractiveIndex', 1.458, 'NA', 1.3, 'c1Excitation', 488, 'c1Emission', 512, ...
%                 'c2Excitation', 561, 'c2Emission', 590, 'MicroscopeType', 'confocal', 'PSF_xy_padding', 2, 'PSF_z_padding', 1, 'XY_symmetric_PSF', 1); end;
if ~exist('MetaData','var'); MetaData = struct('PixelSize', 160, 'zStep', 400, 'RefractiveIndex', 1.515, 'NA', 1.49, 'c1Excitation', 488, 'c1Emission', 512, ...
                 'c2Excitation', 561, 'c2Emission', 590, 'MicroscopeType', 'confocal', 'PSF_xy_padding', 2, 'PSF_z_padding', 1, 'XY_symmetric_PSF', 1); end;

%%%%%%%%%%%%%%% Define the current dataset and parameters %%%%%%%%%%%%%%%%%
if DataSet == 1
    % STEP 1: define the dataset and where to find the images:
    MetaData.MovieData = struct('Path', pwd, 'c1_name', 'OR3_SmallStack_2TimeSteps.tif', 'c1_zPlanes', 12, ...
                                 'c2_name', 'TetR_SmallStack_2TimeSteps.tif', 'c2_zPlanes', 15, 'DataType',  '2TIFFs', 'TimeStep', 2);
    % STEP 2: Define Filtering Parameters (Generally use Laplacian of Gaussian; units are in Pixels):
    MetaData.FilterOptions = struct('FilterType', 'LoG', 'c1_radius', 12, 'c1_sigma', 4.5, 'c1_zPlanes', 3, ...
                                    'c2_radius', 9, 'c2_sigma', 2.5, 'c2_zPlanes', 3);
    % STEP 3: Define thresholds and methods for Dot Dectection (threshold is percentile, e.g. 99.5) 
    %         and available methods are nonMaxSuppression and ConnectedComponents. Padding increases NonMaxSuppr range (PixelUnits):
    MetaData.DectectionOptions = struct('c1_method', 'nonMaxSuppression', 'c1_threshold', 99.5, 'c1_xy_padding', 10, 'c1_z_padding', 1,  ...
                                        'c1_RingRadius', 6, 'c1_QC_thres', 3,  ...
                                        'c2_method', 'ConnectedComponents', 'c2_threshold', 99.95, 'c2_xy_padding', 10, 'c2_z_padding', 1, ...
                                        'c2_RingRadius', 6, 'c2_QC_thres', 3);
                                    
elseif DataSet == 2 % 20180417_C36_300ms_40-488_8-561_CORRECT_1X_cell02
    % STEP 1: define the dataset and where to find the images:
    MetaData.MovieData = struct('Path', '/Users/anderssejrhansen/Dropbox/DataStorage/MicroscopyData/LocusImaging/20180417_C27_C36_Zoncu_SDC/', ...
                            'c1_name', '20180417_C36_300ms_40-488_8-561_CORRECT_1X_cell04_veryGood.tif', 'c1_zPlanes', 19, ...
                                 'c2_name', '20180417_C36_300ms_40-488_8-561_CORRECT_1X_cell04_veryGood.tif', 'c2_zPlanes', 19, 'DataType',  'ZoncuSDC', 'TimeStep', 0.75, 'tPoints', 18);
    % STEP 2: Define Filtering Parameters (Generally use Laplacian of Gaussian; units are in Pixels):
    MetaData.FilterOptions = struct('FilterType', 'LoG', 'c1_radius', 7, 'c1_sigma', 3, 'c1_zPlanes', 3, ...
                                    'c2_radius', 5, 'c2_sigma', 2, 'c2_zPlanes', 3);
    % STEP 3: Define thresholds and methods for Dot Dectection (threshold is percentile, e.g. 99.5) 
    %         and available methods are nonMaxSuppression and ConnectedComponents. Padding increases NonMaxSuppr range (PixelUnits):
    MetaData.DectectionOptions = struct('c1_method', 'ConnectedComponents', 'c1_threshold', 99.75, 'c1_xy_padding', 10, 'c1_z_padding', 1,  ...
                                        'c1_RingRadius', 5, 'c1_QC_thres', 2,  ...
                                        'c2_method', 'ConnectedComponents', 'c2_threshold', 99.975, 'c2_xy_padding', 10, 'c2_z_padding', 1, ...
                                        'c2_RingRadius', 5, 'c2_QC_thres', 4);
    % STEP 5: Define options for tracking: Accepted methods: 'Hungarian', 'NearestNeighbor'
    MetaData.TrackingOptions = struct('MaxGaps', 1, 'MinLocFrac', 0.5, 'MatchMaxDist', 2000,... % these options apply to both colors
                                        'c1_method', 'Hungarian', 'c1_max_link_dist', 3000, ...
                                        'c2_method', 'Hungarian', 'c2_max_link_dist', 3000);
    % STEP 6 (and beyond): Define options for saving:
    MetaData.SaveOptions = struct('PlotPath', MetaData.MovieData.Path, 'PlotPreName', MetaData.MovieData.c1_name, 'SaveMATpath', ['.', filesep, 'TrackedPairsData', filesep, '20180417_C36_ZoncuSDC', filesep]);

end


%%%%%%%%%%%%%%%%%%% STEP 1 - READ IN THE IMAGES/MOVIES %%%%%%%%%%%%%%%%%%%%
if ~exist('FrameData','var') % only read in the movie if you haven't already
    [FrameData] = ASH_ReadMovie(MetaData);
end

%%%%%%%%%%%%% STEP 2 - FILTER THE IMAGES FOR CONTRAST ENHANCEMENT %%%%%%%%%
[FrameData] = ASH_FilterImages(FrameData, MetaData, CurrStep);


%%%%%%%%%%%%% STEP 3 - FIND THE APPROXIMATE POSITION OF ALL DOTS %%%%%%%%%%
% SubSteps (inside the function):
%   3.1: generate the theoretical PSFs
%   3.2: threshold zStacks and find the dots
%   3.3: QC dots by comparing center intensity to a ring around the dot
%   3.4: Plot the success of the dot localization
[FrameData, MetaData] = ASH_LocateTheDots(FrameData, MetaData, CurrStep);

%%%%%%%%%%%%% STEP 4 - PSF-FITTING TO EXTRACT PRECISE 3D XYZ POSITION %%%%%
% SubSteps (inside the function):
%   4.1: extract 3D environment around each spot
%   4.2: determine center-of-mass for PSF-fitting starting guess
%   4.3: least-squares PSF-fit of the 3D dot. 
if CurrStep > 1 
    [FrameData, MetaData] = ASH_3D_PSF_Fit_Dots(FrameData, MetaData);
end

%%%%%%%%%%%% STEP 5 - TRACKS DOTS IN SINGLE-COLOR CHANNELS FIRST %%%%%%%%%%
% SubSteps (inside the function):
%   5.1: Convert to the right format
%   5.2: Connect dots between frames in single color channels
%   5.3: Plot the results to show success of the tracking
if CurrStep > 1 
    [TrackData] = ASH_SingleColorTrack3D(FrameData, MetaData, CurrStep);
end

%%%%%%%%%%%% STEP 6 - MATCH PAIRS OF TRACKED DOTS %%%%%%%%%%%%%%%%%%%%%%%%%
% SubSteps (inside the function):
%   6.1: generate 3D matrix of distances
%   6.2: Iteratively match dots
%   6.3: Plot all the matched trajectories
if CurrStep > 1 
    [TrackedDotPairs, TrackData, FrameData] = ASH_MatchTrackedDots(FrameData, MetaData, TrackData, Step6Plots);
end

%%%%%%%%% STEP 7 - PLOT, SAVE AND QC EACH PAIR OF TRACKED DOTS %%%%%%%%%%%%
% SubSteps (inside the function):
%   7.1: plot img and PSF for each loc in XY, XZ and YZ. 
if CurrStep > 1 
    [TrackedDotPairs] = ASH_PLOT_QC_Save(MetaData, TrackedDotPairs, Step7Plots);
    close all; 
end
