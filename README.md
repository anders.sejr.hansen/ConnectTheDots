ConnectTheDots
--------------------------
written and maintained by
[Anders Sejr Hansen](https://anderssejrhansen.wordpress.com/)

# Brief overview of ConnectTheDots
This repository contains code for ConnectTheDots, a comprehensive
Matlab package for tracking pairs of fluorescently tagged loci over
time. The code takes a input 2-color time-lapse z-stacks, where 2 loci
on the same chromosome have been tagged with 2 different fluorescent
binding proteins (e.g. TetO/TetR vs. LacO/LacI). From the z-stacks, it
will then locate the dots in each color based on user-defined
threshold and precisely determine the dot location using fine-scale
PSF-fitting. After this, it will track dots (i.e. connect the same
dots across time) in each color. Once single-color tracking is
finished, it'll match pairs of loci and quantify their 3D distance
from each other as a function of time.

The code relies on about 40 dependent functions, many of which were
written by
[Anders Sejr Hansen](https://anderssejrhansen.wordpress.com/) and many
of which were written by other people. Please see the Acknowledgements
section for full details.

## General logic of ConnectTheDots
I have attempted to write ConnectTheDots in a highly modular and somewhat
object-oriented manner. All the neccesary information about the movie
(e.g. name, location, PixelStep, TimeStep, thresholds, etc.) is stored in a
structured array object, `MetaData`. After the movie has been read
in, raw and filtered images and the XYZ-coordinates of located dots
are stored in `FrameData`, another structured array object. Once dots
have been located and tracked between frames, single-color tracked
dots are stored in `TrackData`, another structured array
object. Finally, once tracked dots in single colors have been matched
into pairs, pairs of sufficient length are stored in `TrackedDotPairs`, yet another structured array
object, which also contains the 3D distances as a function of
time. Finally, ConnectTheDots will save a MAT-file with `MetaData` and
`TrackedDotPairs`.

The whole procedure is divided into 7 modular steps:

* **STEP 1**: Read in the images/movie using `ASH_ReadMovie.m` and sub-functions.
* **STEP 2**: Enhance contrast in images using `ASH_FilterImages.m` and
sub-functions.
* **STEP 3**: Locate the approximate position of all dots using
  `ASH_LocateTheDots.m` and sub-functions. Display figures to optimize thresholds.
* **STEP 4**: Determine the precise XYZ-coordinates of QC'ed dots using
  PSF-fitting with `ASH_3D_PSF_Fit_Dots.m` and sub-functions
* **STEP 5**: Connect dots across time by 3D tracking in a single color
  using `ASH_SingleColorTrack3D.m` and sub-functions.  Display figure
  of 3D tracking.
* **STEP 6**: Match pairs of tracked dots using `ASH_MatchTrackedDots.m`
  and sub-functions. Calculate 3D distances and plot a 2-color movie
  with tracking overlaid on images. 
* **STEP 7**: Plot PSF-fits and trajectory summaries and save the results
  using `ASH_PLOT_QC_Save.m`. 

When analyzing a movie, there are 2 steps. These are controlled by
setting `CurrStep = 1` and `CurrStep = 2`, respectively.
Use `CurrStep = 1` to go through the first 3 steps and plot the success of image
filtering and the dot detection. This is very helpful for optimizing
the various thresholds and settings. Once the optimal settings have
been identified, proceed to `CurrStep = 2` to do PSF-fitting, tracking
and to save the results. 

# Description of each step
Below we will discuss in more detail each step of ConnectTheDots, the
relevant parameters, how to pick them and how the various
sub-functions work.

## STEP 0 - Preamble
Before moving on to the actual steps performed by ConnectTheDots, it
is neccesary for the user to supply the code with some basic
information about the microscope, the fluorophores, etc. This
information must be provided to `MetaData`, a structured array
object. Specifically, the following information must be provided:

* `MetaData.PixelSize`: double, size of the microscope pixel in
nm.
* `MetaData.zStep`: double, axial length between the z-stacks in nm.
* `MetaData.RefractiveIndex`: double, refractive index of the
  immersion oil. 
* `MetaData.NA`: double, numerical aperature of the microscope objective. 
* `MetaData.c1Excitation`: double, excitation wavelength in units of
nm used to  excite the color 1 fluorophore.
* `MetaData.c2Excitation`: double, excitation wavelength in units of
  nm used to  excite the color 2 fluorophore.
* `MetaData.c1Emission`: double, emission max wavelength in units of
nm of the color 1 fluorophore.
* `MetaData.c2Emission`: double, emission max wavelength in units of
nm of the color 2 fluorophore. 
* `MetaData.MicroscopeType`: string, either `confocal` or `widefield`
  is supported.
* `MetaData.PSF_xy_padding`: integer, extra X,Y padding in units of
pixels used for the PSF-fit.
* `MetaData.PSF_z_padding`: integer, extra Z padding in units of
  pixels used for the PSF-fit. 
* `MetaData.XY_symmetric_PSF`: Boolean integer, use `=1` if the PSF
  should be forced to be symmetric in X and Y during the PSF-fitting
  and use `=0` if X,Y asymmetric PSFs are allowed during the
  PSF-fitting. 


## STEP 1 - Read in the images/movie
In step 1, the movie is read in. How this is done will depend on the
fileformat. For TIFF-stacks, ConnectTheDots uses the function
`tiffread.m` by
[Francois Nedelec](http://www.cytosim.org/misc/index.html). Movies are
read in using  `ASH_ReadMovie.m` and the details of how they are read
are specified in the sub-fields of
`MetaData.MovieData.m`. Specifically,

* `MetaData.MovieData.Path`: string, location of the movie,
e.g. "/User/HardDrive/Data/...".
* `MetaData.MovieData.c1_name`: string, name of the file(s) containing the color
1 images.
* `MetaData.MovieData.c2_name`: string, name of the file(s) containing the color
  2 images.
* `MetaData.MovieData.c1_zPlanes`: integer, number of z-stack planes
for color 1.
* `MetaData.MovieData.c2_zPlanes`: integer, number of z-stack planes
  for color 2.
* `MetaData.MovieData.DataType`: string, argument to specify how the
  movie will be read. Will likely have to be custom-written for each
  file format. 
* `MetaData.MovieData.TimeStep`: double, time between frames in units
  of minutes
* `MetaData.MovieData.tPoints`: integer, total number of timepoints in
  the movie.

At the end of Step 1, all of the images are stored in the structured
array object `FrameData`, which has a size of (N,1), where N is the
number of timepoints. The images are stored as 3D matrices in
`FrameData(N,1).c1_img` and `FrameData(N,1).c2_img`.

## STEP 2 - Contrast enhancement
Contrast enhancement is essential to distinguish real dots from image
noise. Currently, ConnectTheDots has only one option implemented for contrast
enhancement, a Laplacian of Gaussian filter (`LoG`). This is a very
standard filter in image processing for contrast enhancement and a
quick
[Google Search](https://www.google.com/search?q=laplacian+of+gaussian+filter)
will yield many useful resources for understanding this filter and how
to choose a kernel. LoG
filtering is achieved using the function `ASH_FilterImages.m`, which
uses the parameters in `MetaData.FilterOptions` to filter the images
in `FrameData`. To adjust the size of the filter and the sigma,
adjust:

* `MetaData.FilterOptions.FilterType`: string, currently only `LoG` is
supported.
* `MetaData.FilterOptions.c1_radius`: integer, size of kernel in X,Y
for color 1 in pixel units. FISH-Quant default is 5 pixels.
* `MetaData.FilterOptions.c2_radius`: integer, size of kernel in X,Y
  for  color 2  in pixel units. FISH-Quant default is 5 pixels.
* `MetaData.FilterOptions.c1_sigma`: double, standard deviation
(sigma) for color 1. FISH-Quant default is (radius-1)/3.
* `MetaData.FilterOptions.c2_sigma`: double, standard deviation
  (sigma) for color 2. FISH-Quant default is (radius-1)/3.
* `MetaData.FilterOptions.c1_zPlanes`: integer, size of kernel in Z
for  color 1  in pixel units. FISH-Quant default is 3 pixels.
* `MetaData.FilterOptions.c2_zPlanes`: integer, size of kernel in Z
  for  color 2  in pixel units. FISH-Quant default is 3 pixels.

The logic of Step 2 is largely based on
[FISH-Quant](https://www.nature.com/articles/nmeth.2406). Another
common filter is a Difference of Gaussians filter (`DoG`), but in my
hands at least, this did not work as well as `LoG` for Anchor3/OR3 and
related ParB/ParS systems, which have high cytoplasmic intensity. So
`DoG` is not currently implemented. The 3D `LoG` filter used here is by
[Nico Battich, Thomas Stoeger and Lucas Pelkmans](https://www.nature.com/articles/nmeth.2657)
and the code also makes use of Matlab's image processing toolbox
(e.g. `imfilter`). 

It is highly recommended to carefully optimize the filtering settings
to minimize false-positive and false-negative detections in
Step 3. After filtering, the filtered images are stored in `FrameData`
as 3D matrices in
`FrameData(N,1).c1_img_filt` and `FrameData(N,1).c2_img_filt`.

## STEP 3 - Locate all dots approximately
Having contrast enhanced the images, we can now locate the
dots. ConnectTheDots applies a 2-level threshold to locate dots and
filter out false-positive detections:

1. a percentile based intensity threshold using either `nonMaxSuppr` or
`ConnectedComponents`.
2. a custom ring-based algorithm to filter out edge-detections from
   real dots. 

Step 3 runs through 4 sub-steps, which all take place inside the
function `ASH_LocateTheDots.m`. The sub-steps are:

* 3.1: generate the theoretical PSFs for later
* 3.2: threshold z-stacks to find the dots
* 3.3: filter out false-positives using a ring-QC algorithm
* 3.4: plot the success of the dot-detection 

The settings for Step 3 are controlled in `MetaData.DectectionOptions`
using:

* `MetaData.DectectionOptions.c1_method`: string, detection method for
color 1. Either `nonMaxSuppression` or `ConnectedComponents`.
* `MetaData.DectectionOptions.c2_method`: string, detection method for
  color 2. Either `nonMaxSuppression` or `ConnectedComponents`.
* `MetaData.DectectionOptions.c1_threshold`: double, percentile
  threshold for what constitutes a dot in color 1. Normally somewhere
  around 99.5-99.99 is a good starting point.
* `MetaData.DectectionOptions.c2_threshold`: double, percentile
  threshold for what constitutes a dot in color 2. Normally somewhere
  around 99.5-99.99 is a good starting point. 
* `MetaData.DectectionOptions.c1_xy_padding`: integer, extra X,Y pixel
  range in  color 1  for the `nonMaxSuppr` method only. Ignored by
  `ConnectedComponents`.
* `MetaData.DectectionOptions.c2_xy_padding`: integer, extra X,Y pixel
  range in  color 2  for the `nonMaxSuppr` method only. Ignored by
  `ConnectedComponents`.
* `MetaData.DectectionOptions.c1_z_padding`: integer, extra Z pixel
  range in  color 1  for the `nonMaxSuppr` method only. Ignored by
  `ConnectedComponents`.
* `MetaData.DectectionOptions.c2_z_padding`: integer, extra Z pixel
  range in  color 2  for the `nonMaxSuppr` method only. Ignored by `ConnectedComponents`.
* `MetaData.DectectionOptions.c1_RingRadius`: integer, radius of the
ring in color 1 used filter out false-positives.
* `MetaData.DectectionOptions.c2_RingRadius`: integer, radius of the
  ring in color 2 used filter out false-positives. 
* `MetaData.DectectionOptions.c1_QC_thres`: double, the minimal neccesary ratio
  of the center dot intensity to the mean ring perimeter intensity in
  order for the dot to be counted in color 1.
* `MetaData.DectectionOptions.c2_QC_thres`: double, the minimal neccesary ratio
  of the center dot intensity to the mean ring perimeter intensity in
  order for the dot to be counted in color 2. 

In Step 3.1, we generate the theoretical PSFs based on the microscope
using the function `ASH_sigma_Theo_PSF.m`, which implements the
Gaussian approximations of the fluorescence microscope PSF as a
function of NA, refractive index, excitation and emission wavelengths
as described in
[Zhang et al.](https://doi.org/10.1364/AO.46.001819). These are used
in Step 4 for the PSF-fitting and in Step 3.2 for the detection.

Next, in Step 3.2 the dots are located using an intensity threshold
specified by a percentile in the function `ASH_ThresholdDots.m`, which
implements 2 methods: `nonMaxSuppression` or
`ConnectedComponents`. This function is closely based on the methods
implemented in
[FISH-Quant](https://bitbucket.org/muellerflorian/fish_quant). The
`nonMaxSuppression` method calls `nonMaxSupr.m`, written by
[Piotr Dollar](https://pdollar.github.io/toolbox/), which in turn
calls `ind2sub2.m`, `nlfiltersep.m` and
`nlfiltersep_max`. `nonMaxSuppression` takes as arguments an image,
radii (vector with X,Y,Z elements) and an intensity threshold and
returns local maxima that exceed the threshold. Although very
powerful in general, `nonMaxSuppression` will fail to detect a dot
that is too close to another dot (as sometimes occurs). Thus, the
radii must be chosen to be smaller than the typical distances between
dots that are near each other (e.g. the 2 alleles inside the same
nucleus).
Conversely, the other implemented method, `ConnectedComponents`,
relies on functions built into the Matlab Image Processing toolbox,
specifically [bwconncomp](https://www.mathworks.com/help/images/ref/bwconncomp.html)
and [regionprops](https://www.mathworks.com/help/images/ref/regionprops.html)
(see the links for full details). Briefly, `ConnectedComponents` takes
a binary image as input (the binary image is constructed using the
user-supplied percentile-based intensity threshold) and then returns
the centroids of each localization. Finally, `ASH_ThresholdDots.m`
ensures that the detected dots are not too close to the image edges to
cause problems during the PSF-fitting. As mentioned, the logic of
Step 3.2 is closely based on and owes a lot to [FISH-Quant](https://bitbucket.org/muellerflorian/fish_quant). 

ConnectTheDots is intended to be used such that relatively lax
thresholds are set in Step 3.2 (many false positives, essentially no
false negatives). Step 3.3 is intended to sort the false postives from
Step 3.2 from the true dots using a ring QC algorithm implemented in
`ASH_RingQC_Dots.m`. This function goes through each detected dots in
Step 3.2 and then draws a ring with radius
`MetaData.DectectionOptions.c1_RingRadius` (or c2) around the dot
centroid in the z-plane with the dot centroid. It then calculates the
background-subtracted mean intensity of the pixels in the ring
perimeter and compares it to the mean background-subtracted intensity
of a 3x3 matrix  centered on the dot centroid. If the dot intensity is
at least `MetaData.DectectionOptions.c1_QC_thres`-fold (or c2) higher
than the perimenter intensity, it gets counted as a true detection. At
least with my experimental settings, this has proven to be a very
effective method for filtering out false positive detections, which it
is otherwise very difficult to do using solely a `DoG` or `LoG`
filter.

Finally, in step 3.4, the success of the dot detections are
plotted. Subplotting with minimal wasted space is achieved using
`subaxis.m` by
[Aslak Grinsted](https://www.mathworks.com/matlabcentral/fileexchange/3696-subaxis-subplot). Maximum
intensity projections in XY are plotted for the first and the last
time-point side-by-side with the filtered images. All detected dots
are shown as green crosses and the dots that also passed ring-QC are
shown in cyan circles. The values of the thresholds are also
displayed. Simulatanously, raw maximum intensity projections are
plotted as large images for both colors and for the first and last
time-points (since sometimes that image annotation can make it
difficult to see the actual dots). The various images are
contrast-adjust in `ASH_LocateTheDots.m` using re-scaling thresholds
reasonable for our images, but these may have to be changed if the
contrast is not right for your images (see lines 130-175 in
`ASH_LocateTheDots.m`). Unless `CurrStep = 1`, the plotting will be
skipped.

## STEP 4 - Least-squares 3D PSF-fitting to extract precise XYZ position
Once the final list of dots have been determined in Step 3, we need to
extract the precise XYZ coordinates using PSF-fitting and this is
achieved in 2 sub-steps within the function
`ASH_3D_PSF_Fit_Dots.m`. The sub-steps are:

* 4.1: crop out 3D image around each dot
* 4.2: Calculate Center-Of-Mass as initial guess and then perform 3D PSF-fitting on cropped images

3D images around each dot are cropped out using
`ASH_Crop3D_ImgAroundDot.m`. Each cropped image is stored in a cell
array inside `FrameData`, e.g. `FrameData(N,1).c1_img_3D_crop` for
color 1.
Next, all the intensive PSF-fitting takes place inside
`ASH_CoM_PSF_fit_each_dot.m`, whose logic is also based on
[FISH-Quant](https://bitbucket.org/muellerflorian/fish_quant). The
PSF-fitting uses a 1x7 or 1x8 vector with
`{'PSF_sigma_XY','PSF_sigma_Z', 'X', 'Y', 'Z', 'Amplitude','Background'}` or `{'PSF_sigma_X','PSF_sigma_Y', 'PSF_sigma_Z', 'X',
'Y', 'Z', 'Amplitude', 'Background'}`, respectively, depending on whether or not the PSF-fit
is set to be symmetric or not in X,Y.
To generate an initial guess for the PSF-fitting parameters, the
theoretical PSF shape from Step 3.1 is used and the initial conditions
for the X,Y,Z coordinates are extracted by calculating the center of
mass of the cropped 3D image using `ait_centroid3d_v3.m` from [FISH-Quant](https://bitbucket.org/muellerflorian/fish_quant). The
PSF-fitting is then performed using Matlab's in-built non-linear
least-squares fitting routine, `lsqcurvefit`. The 3D PSF fit in linear
coordinates is calculated using `fun_Gaussian_3D_v2.m` from
[FISH-Quant](https://bitbucket.org/muellerflorian/fish_quant), which
also calls `erf2.m`. Specifically,
all of the fitting and least-squares optimization is performed on
linear coordinates instead of on 3D images using Matlab's `meshgrid`
function. After the PSF-fitting, the fitted images are also generated and saved
to a cell array. These will be used in Step 7.2 to visualize the
PSF-fit side-by-side with the raw image. Finally, the precisely fit
XYZ coordinates are saved to `FrameData(N,1).FinalDots_c1` (or c2) as
a Nx3 matrix with XYZ coordinates.

## STEP 5 - single color 3D tracking
Now that we have located all of the dots in each color and precisely
determined the XYZ coordinates at each time point, we need to connect
the dots across time. This process is called tracking. And we need to
convert a list of localizations in each frame to a list of
trajectories, each of which contains the XYZ coordinates of the same
dot in each time-frame. All the single-color tracking is performed in 
`ASH_SingleColorTrack3D.m`. This functions has 3 sub-steps:

* 5.1: convert to a cell array format
* 5.2: perform tracking
* 5.3: plot the outcome of the tracking

First, in Step 5.1, `ASH_Convert2ToCellArray.m` converts the
localizations in both colors to cell arrays and stores these in the
new structured array, `TrackData`. Once converted, the tracking is
handled by `ASH_3D_track.m` in Step 5.2. The actual tracking is
performed using the function, `simpletracker.m`, written by [Jean-Yves
Tineviz](https://scholar.google.com/citations?user=-pk1vDIAAAAJ&hl=en)
and hosted on
[FileExchange](https://www.mathworks.com/matlabcentral/fileexchange/34040-simple-tracker). `simpletracker.m`
implements a simple `NearestNeighbor` method as well as the [Hungarian
algorithm](https://en.wikipedia.org/wiki/Hungarian_algorithm) using
`Hungarian`. Briefly, the  [Hungarian
algorithm](https://en.wikipedia.org/wiki/Hungarian_algorithm)
minimizes the total sum of pair distances between frames and is the
preferred default method in `ConnectTheDots`. Specifically, the
tracking parameters are controlled by the settings in the structured
array object `MetaData.TrackingOptions`, where the following fields
must be specificed:

* `MetaData.TrackingOptions.MaxGaps`: integer, max number of allowed
  gaps (e.g. if the dots were localized in frames 1,3,4... they will
  still be connected even though frame 2 is missing if
  MaxGaps>0). Default is `MaxGaps=1`. Applies to both colors.
* `MetaData.TrackingOptions.MinLocFrac`: double [0;1], specifies the
  minimal fraction of the total frames in which the dot must have been
  located in order for it to be counted as a `LongTrack`. Default
  is 0.5. Applies to both colors.
* `MetaData.TrackingOptions.c1_method`: string, specifies the tracking
  method for color 1. Either `Hungarian` or `NearestNeighbor` is
  supported. Default is `Hungarian`.
* `MetaData.TrackingOptions.c2_method`: string, specifies the tracking
  method for color 2. Either `Hungarian` or `NearestNeighbor` is
  supported. Default is `Hungarian`.
* `MetaData.TrackingOptions.c1_max_link_dist`: double, maximal allowed
  3D distance between frames in units of nm for color 1. Mouse
  embryonic stem cells are quite mobile, so the default is 3000 nm.
* `MetaData.TrackingOptions.c2_max_link_dist`: double, maximal allowed
  3D distance between frames in units of nm for color 2. Mouse
  embryonic stem cells are quite mobile, so the default is 3000 nm. 

After tracking, all trajectories regardless of length are stored in
the structured array object `AllTracks`, whereas the ones of
sufficient length (cf. `MetaData.TrackingOptions.MinLocFrac`) are
stored in `LongTracks`.
Finally, in Step 5.3, the single-color 3D trajectories are
plotted. The 3D plotting is facilitated by the function
[plotcube.m](https://www.mathworks.com/matlabcentral/fileexchange/15161-plotcube).

## STEP 6 - Match pairs of tracked dots
Once the dots have been connected in a single color in Step 5, they
need to be matched into pairs, corresponding to differently colored
(e.g. green and red) dots on the same chromosome. This is achieved
using `ASH_MatchTrackedDots.m` and 3 sub-steps performed inside that
function:

* 6.1: generate matrix of 3D distances
* 6.2: Iteratively match pairs of dots and calculate 3D distances
* 6.3: Plot a movie of tracking 

In Step 6.1, all 3D distances between dots are calculated using
`ASH_CalcTimeAveraged3D_Dist.m`. This generates a matrix,
`Matrix3D_Dist`, which is used in Step 6.2.
In Step 6.2, we use a custom iterative approach to match pairs of dot
trajectories. This approach is implemented in
`ASH_IterativelyMatchPairsOfDots.m`. First, we calculate the
time-averaged distance between pairs of dots. This is important
because the total distance would otherwise be artificially shorter for
shorter trajectories. `NaNs` and `nanmean` are used to handle missing
values. The maximal time-averaged pair distance is determined using
`MetaData.TrackingOptions.MatchMaxDist` (default value is 2000 nm). We
then run a `while` loop matching the pair with the shortest
time-averaged distance first and continuing up until the time-averaged
distance exceeds the threshold,
`MetaData.TrackingOptions.MatchMaxDist`. Moreover, since there could
be gaps in one color, but not in the other, this is handled by
removing frames with a localization missing in one or both colors. Finally, the 3D distance
between frames is also calculated for each matched pair. The final
matched pairs of dots and their trajectories are then saved to the
structured array object, `TrackedDotPairs`.

Finally, provided that `Step6Plots = 1`, Step 6.3 will plot a movie of
the tracking. Specifically, for each timepoint, 4 subplots are
plotted using the function `subaxis.m`. The first 2 subplots show
color 1 and color 2 maximum intensity projections (MIPs) with the tracked
localizations overlaid. To do this, `ASH_MatchTrackedDots.m`
contrast-adjusts the MIPs and converts them to `uint8 RGB`. If the
contrast in your images is very different, you may need to change the
contrast settings in lines 75-100 of `ASH_MatchTrackedDots.m`. The 3rd
subplot shows the 2 color channels overlaid. Color 1 is shown in green
and color 2 is shown in magenta, so perfect co-localization will
appear white. The 4th subplot shows that 3D distance as a function of
time. For each time point, a 300 dpi PNG is saved. These can easily be
converted to a movie using the `Import>Image Sequence` function in
[ImageJ](https://imagej.nih.gov/ij/index.html) or
[FIJI](https://fiji.sc/) (note that FIJI is just ImageJ with a series
of functions/packages pre-installed).
These images will be saved to a user-specified directory. Control this
using `MetaData.SaveOptions`:

* `MetaData.SaveOptions.PlotPath`: string, path for the directory
  where the images in Step 6.3 should be saved to. Default is
  `MetaData.MovieData.Path`. 
* `MetaData.SaveOptions.PlotPreName`: string, prefix for the saved
  PNGs. 

## STEP 7 - QC, plot PSF-fits and save the data
Finally, in Step 7, we will finish up. Step 7 is handled by
`ASH_PLOT_QC_Save.m`, wherein 4 sub-steps take place:

* 7.1: Calculate the signal-to-background ratio of each localization
* 7.2: Plot the raw image of each localization and the PSF-fit (to
assess the quality of the localization)
* 7.3: Plot overall summary of each trajectory
* 7.4: Save a MAT-file with `MetaData` and `TrackedDotPairs`.

Now that we have narrowed down the final list of localizations to the
ones contained in `TrackedDotPairs`, we go through each localization
in Step 7.1 and calculate the signal-to-background ratio in
`ASH_Calc_Signal2Background.m`. We calculate this by comparing the
total signal to the fitted background in the small cropped image.
In Step 7.2, we plot the Maximum Intensity Projections (MIPs) of the
raw 3D cropped images in XY, XZ and YZ with the fitted localization
coordinates overlaid side-by-side with the fitted PSF image. This is
quite helpful when it comes to assessing the quality of the image and
the quality of the PSF-fit. Specifically, it is highly useful for
detecting loci that have undergone DNA replication (1 dot --> 2 dots,
which partially overlap and which need to be weeded out). It is also useful for determining whether
the microscope PSF is corrupted by experimental aberrations. All of
this is performed inside `ASH_PLOT_QC_Save.m`. This part of the code
is quite slow and will only occur if `Step7Plots = 1`. It uses the
function [export_fig](https://github.com/altmany/export_fig) to save
multiple pages as a single PDF and saves one PDF per
trajectory. [export_fig](https://github.com/altmany/export_fig) is
written and maintained by Yair Altman. Please note that `export_fig`
can cause issues if GhostScript is not installed or
out-of-date. Please see
[GitHub](https://github.com/altmany/export_fig) for details. 
Next, in Step 7.3, a summary plot is saved for each paired trajectory
with the 3D distance vs. time and the signal-to-background vs. time
for both colors plotted. Also uses `export_fig.m` and `subaxis.m`.
Finally, in Step 7.4, a MAT-file containing the 2 key structured array
objects, `MetaData` and `TrackedDotPairs`, is saved using the prefix
name defined in `MetaData.MovieData.c1_name` to the directory defined
in `MetaData.SaveOptions.SaveMATpath`. 

At the end of Step 7, the Matlab workspace should be cleared using `>>clear` before a new movie is analyzed. 

## Issues and compatibility problems
The code has been tested on a Mac running Matlab 2014b. It has not
been tested on other platforms or with other versions of Matlab. The
code appends PDFs using `export_fig`, which requires GhostScript to be
installed (please see
[GitHub](https://github.com/altmany/export_fig)). Please see [this page](http://pages.uoregon.edu/koch)
where you can download and install ghoscript if you have trouble with this
step. 

## License
These programs are released under the GNU General Public License version 3 or upper (GPLv3+).

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


## Acknowledgements

This project makes heavy use of a large number of open-source
functions written by other people.
ConnectTheDots is especially indebted to Florian Mueller and Herve
Marie Nelly et al. for [FISH-Quant](https://bitbucket.org/muellerflorian/fish_quant). The logic of steps 2-4 (image
filtering; dot detections; PSF-fitting) is largely based on the logic
implemented in [FISH-Quant](https://bitbucket.org/muellerflorian/fish_quant). I would also like to thank Florian for
trouble-shooting help during the early stages of writing
ConnectTheDots. The single-color tracking is performed using Jean-Yves Tineviz's
[Hungarian algorithm implementation](https://www.mathworks.com/matlabcentral/fileexchange/34040-simple-tracker). Full
details on all the open-source functions used by `ConnectTheDots` is
given below:

1. **STEP 1 functions**:
   * `ASH_ReadMovie.m` written by
   [Anders Sejr Hansen](https://anderssejrhansen.wordpress.com/).
   * `tiffread.m` written by [Francois Nedelec](http://www.cytosim.org/misc/index.html) and Woods Hole
     physiology course. A newer version is on the [FileExchange](https://www.mathworks.com/matlabcentral/fileexchange/10298-tiffread2-m).

2. **Step 2 functions**:
	 * `ASH_FilterImages.m`written by
	 [Anders Sejr Hansen](https://anderssejrhansen.wordpress.com/).
	 * `fspecialCP3D.m` written by [Nico Battich, Thomas Stoeger and
       Lucas Pelkmans](https://www.nature.com/articles/nmeth.2657). 

3. **Step 3 functions**:
	 * `ASH_LocateTheDots.m` written by
	 [Anders Sejr Hansen](https://anderssejrhansen.wordpress.com/).
	 * `ASH_ThresholdDots.m`written by
	 [Anders Sejr Hansen](https://anderssejrhansen.wordpress.com/).
	 * `nonMaxSupr.m` written by Piotr Dollar as part of his
     [Toolbox](https://pdollar.github.io/toolbox/).
	 * `ind2sub2.m` written by Piotr Dollar as part of his
     [Toolbox](https://pdollar.github.io/toolbox/).
	 * `nlfiltersep.m` written by Piotr Dollar as part of his
     [Toolbox](https://pdollar.github.io/toolbox/).
	 * `nlfiltersep_max.m` written by Piotr Dollar as part of his
     [Toolbox](https://pdollar.github.io/toolbox/).
	 * `ASH_RingQC_Dots.m` written by
	 [Anders Sejr Hansen](https://anderssejrhansen.wordpress.com/).
	 * `subaxis.m` and `parseArgs.m` written by [Aslak Grinsted](http://www.glaciology.net/) and
       available at the [FileExchange](https://www.mathworks.com/matlabcentral/fileexchange/3696-subaxis-subplot).
	 * Many functions from Matlab's image processing toolbox including
       `bwconncomp` and `regionprops`.

4. **Step 4 functions**:
	 * `ASH_Crop3D_ImgAroundDot.m` written by
	 [Anders Sejr Hansen](https://anderssejrhansen.wordpress.com/).
	 * `ASH_CoM_PSF_fit_each_dot.m` written by
	 [Anders Sejr Hansen](https://anderssejrhansen.wordpress.com/).
	 * `ait_centroid3d_v3.m` written by Herve Marie Nelly as part of
     [FISH-Quant](https://bitbucket.org/muellerflorian/fish_quant).
	 * `lsqcurvefit` from Matlab's optimization Toolbox.
	 * `fun_Gaussian_3D_v2.m` written by Florian Mueller and Herve
       Marie Nelly as part of
       [FISH-Quant](https://bitbucket.org/muellerflorian/fish_quant).
	 * `erf2.m` also from [FISH-Quant](https://bitbucket.org/muellerflorian/fish_quant).

5. **Step 5 functions**:
	 * `ASH_SingleColorTrack3D.m` written by
	 [Anders Sejr Hansen](https://anderssejrhansen.wordpress.com/).
	 * `ASH_Convert2ToCellArray.m` written by
	 [Anders Sejr Hansen](https://anderssejrhansen.wordpress.com/).
	 * `ASH_3D_track.m`  written by
	 [Anders Sejr Hansen](https://anderssejrhansen.wordpress.com/).
	 * `simpletracker.m` written by
       [Jean-Yves Tineviz](https://scholar.google.com/citations?user=-pk1vDIAAAAJ&hl=en)
       and hosted on
       [FileExchange](https://www.mathworks.com/matlabcentral/fileexchange/34040-simple-tracker).
	 * `plotcube.m` written by Olivier and hosted on [FileExchange](https://www.mathworks.com/matlabcentral/fileexchange/15161-plotcube).

6. **Step 6 functions**:
	 * `ASH_MatchTrackedDots.m` written by
	 [Anders Sejr Hansen](https://anderssejrhansen.wordpress.com/).
	 * `ASH_CalcTimeAveraged3D_Dist.m` written by
	 [Anders Sejr Hansen](https://anderssejrhansen.wordpress.com/).
	 * `ASH_IterativelyMatchPairsOfDots.m` written by
	 [Anders Sejr Hansen](https://anderssejrhansen.wordpress.com/).
	 * `subaxis.m` and `parseArgs.m` written by [Aslak Grinsted](http://www.glaciology.net/) and
       available at the [FileExchange](https://www.mathworks.com/matlabcentral/fileexchange/3696-subaxis-subplot)

7. **Step 7 functions**:
   * `ASH_PLOT_QC_Save.m` written by
   [Anders Sejr Hansen](https://anderssejrhansen.wordpress.com/).
   * `ASH_Calc_Signal2Background.m` written by
   [Anders Sejr Hansen](https://anderssejrhansen.wordpress.com/).
   * `export_fig.m` by
     [Yair Altman](https://undocumentedmatlab.com/about) and hosted on
     [GitHub](https://github.com/altmany/export_fig). Note that this
     requires GhostScript to be installed.

## References

	Battich, N., Stoeger, T. and Pelkmans, L., 2013.
	Image-based transcriptomics in thousands of single human cells at
	single-molecule resolution.
	Nature methods, 10(11), p.1127.
	
	Mueller, F., Senecal, A., Tantale, K., Marie-Nelly, H., Ly, N.,
	Collin, O., Basyuk, E., Bertrand, E., Darzacq, X. and Zimmer,
	C., 2013.
	FISH-quant: automatic counting of transcripts in 3D FISH
	images.
	Nature methods, 10(4), p.277.

	Schindelin, J., Arganda-Carreras, I., Frise, E., Kaynig, V.,
	Longair, M., Pietzsch, T., Preibisch, S., Rueden, C., Saalfeld,
	S., Schmid, B. and Tinevez, J.Y., 2012.
	Fiji: an open-source platform for biological-image analysis.
	Nature methods, 9(7), p.676.

	Zhang, B., Zerubia, J. and Olivo-Marin, J.C., 2007.
	Gaussian approximations of fluorescence microscope point-spread
	function models.
	Applied optics, 46(10), pp.1819-1829.


