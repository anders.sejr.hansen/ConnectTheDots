function [TrackedDotPairs] = ASH_IterativelyMatchPairsOfDots(Tracks1, Tracks2, Matrix3D_Dist, MatchMaxDist)
%ASH_ITERATIVELYMATCHPAIRSOFDOTS Iteratively match dots into pairs using a
%max matching distance
%   Written by Anders Sejr Hansen

% Take time-averaged mean using NanMean, which can handle NaN values and
% thus only considers the non-NaN elements:
avgMatrix3D_Dist = nanmean(Matrix3D_Dist,3);

% Iterative pair matching algorithm:
%   1. find the pair with the lowest time-averaged distance
%   2. match this pair and remove it from the list
%   3. Repeat to find the next pair and continue until you run out of dots
%   or until time-average minimal distance exceeds MatchMaxDist
% store everything in TrackedDotPairs
TrackedDotPairs = struct();

TimeToStop = false; numPairs = 1;
while TimeToStop == false
    %%%%%%%% FIND A PAIR TO MATCH %%%%%%%%%%%%
    % find the current minimun time-averaged distance:
    [minDist,minIdx]=min(avgMatrix3D_Dist(:));
    % get the traj# of both dots:
    [c1_idx,c2_idx]=ind2sub(size(avgMatrix3D_Dist),minIdx);
    
    if minDist > MatchMaxDist
        % the distance is larger than the max, so abort
        %TimeToStop = true;
        break;
    else
        %%%%%%%%% SAVE ALL THE RELEVANT INFO %%%%%%%%%%%%%%%%%%
        % color 1:
        TrackedDotPairs(numPairs,1).c1.XYZ = Tracks1(c1_idx,1).XYZ;
        TrackedDotPairs(numPairs,1).c1.Frames = Tracks1(c1_idx,1).Frames;
        TrackedDotPairs(numPairs,1).c1.TimeStamp = Tracks1(c1_idx,1).TimeStamp;
        TrackedDotPairs(numPairs,1).c1.PSF_sigma_XYZ_nm = Tracks1(c1_idx,1).PSF_sigma_XYZ_nm;
        TrackedDotPairs(numPairs,1).c1.Amplitude = Tracks1(c1_idx,1).Amplitude;
        TrackedDotPairs(numPairs,1).c1.Background = Tracks1(c1_idx,1).Background;
        TrackedDotPairs(numPairs,1).c1.img_3D_crop = Tracks1(c1_idx,1).img_3D_crop;
        TrackedDotPairs(numPairs,1).c1.img_fit_3D_crop = Tracks1(c1_idx,1).img_fit_3D_crop;
        TrackedDotPairs(numPairs,1).c1.img_3D_crop_PSF_XYZ = Tracks1(c1_idx,1).img_3D_crop_PSF_XYZ;
        TrackedDotPairs(numPairs,1).c1.ID = c1_idx;
        % color 2:
        TrackedDotPairs(numPairs,1).c2.XYZ = Tracks2(c2_idx,1).XYZ;
        TrackedDotPairs(numPairs,1).c2.Frames = Tracks2(c2_idx,1).Frames;
        TrackedDotPairs(numPairs,1).c2.TimeStamp = Tracks2(c2_idx,1).TimeStamp;
        TrackedDotPairs(numPairs,1).c2.PSF_sigma_XYZ_nm = Tracks2(c2_idx,1).PSF_sigma_XYZ_nm;
        TrackedDotPairs(numPairs,1).c2.Amplitude = Tracks2(c2_idx,1).Amplitude;
        TrackedDotPairs(numPairs,1).c2.Background = Tracks2(c2_idx,1).Background;
        TrackedDotPairs(numPairs,1).c2.img_3D_crop = Tracks2(c2_idx,1).img_3D_crop;
        TrackedDotPairs(numPairs,1).c2.img_fit_3D_crop = Tracks2(c2_idx,1).img_fit_3D_crop;
        TrackedDotPairs(numPairs,1).c2.img_3D_crop_PSF_XYZ = Tracks2(c2_idx,1).img_3D_crop_PSF_XYZ;
        TrackedDotPairs(numPairs,1).c2.ID = c2_idx;
        % now the gaps may not match: e.g. color 1 could be there in frames
        % 1,2,4 and color 2 in frames 1,3,4. In this case frames 2,3 are kinda
        % useless and we only really need to consider frames 1,4 for the paired
        % dots. So get rid of the frames where both particles were not
        % localized:
        [match_Frames, match_c1_idx, match_c2_idx] = intersect(Tracks1(c1_idx,1).Frames, Tracks2(c2_idx,1).Frames);
        % this finds both the intersecting values and indices

        % pre-declare matched variables:
        c1_XYZ = Tracks1(c1_idx,1).XYZ(match_c1_idx,:);
        c2_XYZ = Tracks2(c2_idx,1).XYZ(match_c2_idx,:);
        TrackedDotPairs(numPairs,1).c1_XYZ = c1_XYZ;
        TrackedDotPairs(numPairs,1).c2_XYZ = c2_XYZ;
        TrackedDotPairs(numPairs,1).Frames = match_Frames;
        TrackedDotPairs(numPairs,1).TimeStamp = Tracks1(c1_idx,1).TimeStamp(match_c1_idx);
        % also calculate the 3D distance 
        Dist3D = zeros(length(match_Frames),1);
        for iter = 1:length(match_Frames)
            Dist3D(iter,1) = pdist2(c1_XYZ(iter,:), c2_XYZ(iter,:));
        end
        % now save the 3D distance:
        TrackedDotPairs(numPairs,1).Dist3D = Dist3D;
        
        % FINISH - now increment the counter:
        numPairs = numPairs + 1;
        
        % Now you need to make sure that these trajectories are not picked
        % again, so articifically set their rows and columns to something
        % very big:
        avgMatrix3D_Dist(c1_idx,:) = 5*MatchMaxDist * ones(1,size(avgMatrix3D_Dist,2));
        avgMatrix3D_Dist(:, c2_idx) = 5*MatchMaxDist * ones(size(avgMatrix3D_Dist,1),1);
    end
end

end

