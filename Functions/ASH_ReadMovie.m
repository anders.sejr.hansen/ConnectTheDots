function [ FrameData ] = ASH_ReadMovie( MetaData )
%READMOVIE read MovieFile 
%   This function takes as input a Structure Object with information about
%   where the MovieFile(s) is and how to read it and then reads it in.
%   The output is a structure array, FrameData, where each timepoint as a
%   3D images for both color c1 and c2

%   Written by Anders Sejr Hansen


FrameData = struct(); % define the Frame-specific structure/object

if strcmp(MetaData.MovieData.DataType, '2TIFFs')
    disp('Step 1: Reading in the Movie files as two separate TIFF files'); tic;
   % In this case, the 2-color movie is stored as TIFF files and the two colors 
   % are stored in separate files. So read in each one sequentially
   
   % read the c1 channel first:
   [stack, nbImages] = tiffread([MetaData.MovieData.Path, filesep, MetaData.MovieData.c1_name]);
   c1_img = zeros(stack(1,1).height, stack(1,1).width, MetaData.MovieData.c1_zPlanes, round(nbImages/MetaData.MovieData.c1_zPlanes));
   
   % now get each slice for the 1st color
   iter = 1;
   for t_iter = 1:round(nbImages/MetaData.MovieData.c1_zPlanes)
       for z_iter = 1:MetaData.MovieData.c1_zPlanes
           c1_img(:,:,z_iter, t_iter) = stack(1,iter).data;
           iter = iter + 1;
       end
   end
      
   % read the c2 channel second:
   [stack, nbImages] = tiffread([MetaData.MovieData.Path, filesep, MetaData.MovieData.c2_name]);
   c2_img = zeros(stack(1,1).height, stack(1,1).width, MetaData.MovieData.c2_zPlanes, round(nbImages/MetaData.MovieData.c2_zPlanes));
   
   % now get each slice for the 2nd color
   iter = 1;
   for t_iter = 1:round(nbImages/MetaData.MovieData.c2_zPlanes)
       for z_iter = 1:MetaData.MovieData.c2_zPlanes
           c2_img(:,:,z_iter, t_iter) = stack(1,iter).data;
           iter = iter + 1;
       end
   end
   
   % finally add the frames to the FrameData structure/object:
   for iter = 1:size(c1_img,4)
       FrameData(iter,1).c1_img = c1_img(:,:,:,iter);
       FrameData(iter,1).c2_img = c2_img(:,:,:,iter);
   end
   toc; % display time

elseif strcmp(MetaData.MovieData.DataType, 'ZoncuSDC')
    disp('Step 1: Reading in the Movie files as Big Zoncu SDC TIFF-stack'); tic;
    max_t = MetaData.MovieData.tPoints;
    % read the both channels first:
   [stack, ~] = tiffread([MetaData.MovieData.Path, filesep, MetaData.MovieData.c1_name]);
   c1_img = zeros(stack(1,1).height, stack(1,1).width, MetaData.MovieData.c1_zPlanes, max_t);
   c2_img = zeros(stack(1,1).height, stack(1,1).width, MetaData.MovieData.c2_zPlanes, max_t);
    
   % now get each slice for the 1st color
   iter = 1;
   for t_iter = 1:max_t
       for z_iter = 1:MetaData.MovieData.c1_zPlanes
           curr_img_iter = 2*(iter-1)+1;
           c1_img(:,:,z_iter, t_iter) = stack(1,curr_img_iter).data;
           iter = iter + 1;
       end
   end
   
   % now get each slice for the 2nd color
   iter = 1;
   for t_iter = 1:max_t
       for z_iter = 1:MetaData.MovieData.c2_zPlanes
           curr_img_iter = 2*(iter-1)+2;
           c2_img(:,:,z_iter, t_iter) = stack(1,curr_img_iter).data;
           iter = iter + 1;
       end
   end

   % finally add the frames to the FrameData structure/object:
   for iter = 1:max_t
       FrameData(iter,1).c1_img = c1_img(:,:,:,iter);
       FrameData(iter,1).c2_img = c2_img(:,:,:,iter);
   end
   
   toc;
   
   
elseif strcmp(MetaData.MovieData.DataType, 'ZoncuSDC_Weird')
    disp('Step 1: Reading in the Movie files as Big Zoncu SDC TIFF-stack'); tic;
    max_t = 15;
    % read the both channels first:
   [stack, ~] = tiffread([MetaData.MovieData.Path, filesep, MetaData.MovieData.c1_name]);
   c1_img = zeros(stack(1,1).height, stack(1,1).width, MetaData.MovieData.c1_zPlanes, max_t);
   c2_img = zeros(stack(1,1).height, stack(1,1).width, MetaData.MovieData.c2_zPlanes, max_t);
    
   % now get each slice for the 1st color
   iter = 1;
   for t_iter = 1:max_t
       for z_iter = 1:MetaData.MovieData.c1_zPlanes
           curr_img_iter = 4*(iter-1)+1;
           c1_img(:,:,z_iter, t_iter) = stack(1,curr_img_iter).data;
           iter = iter + 1;
       end
   end
   
   % now get each slice for the 2nd color
   iter = 1;
   for t_iter = 1:max_t
       for z_iter = 1:MetaData.MovieData.c2_zPlanes
           curr_img_iter = 4*(iter-1)+2;
           c2_img(:,:,z_iter, t_iter) = stack(1,curr_img_iter).data;
           iter = iter + 1;
       end
   end
   
   % finally add the frames to the FrameData structure/object:
   for iter = 1:size(c1_img,4)
       FrameData(iter,1).c1_img = c1_img(:,:,:,iter);
       FrameData(iter,1).c2_img = c2_img(:,:,:,iter);
   end
    
   toc;
   
   
   
elseif strcmp(MetaData.MovieData.DataType, 'testy')
    disp('Step 1: Reading in the Movie files as testing individual TIFF files'); tic;
    
    % r02c06f01p09-ch1sk1fk1fl1.tiff
    txt_pre = 'r02c06f01';
    txt_end = 'fk1fl1.tiff';
    txt_color = '-ch2';
    max_t = 5; % max number of time points
    max_planes = 20;
    % read the c1-channel
    first_frame = 1;
    for t_iter=1:max_t
        for p_iter = 1:max_planes
            % make the name:
            if p_iter < 10
                txt_plane = ['p0', num2str(p_iter)];
            elseif p_iter < 100
                txt_plane = ['p', num2str(p_iter)];
            end
            txt_time = ['sk', num2str(t_iter)];
            
            %define the current name:
            curr_name = [txt_pre, txt_plane, txt_color, txt_time, txt_end];
            curr_img = double(imread([MetaData.MovieData.Path, filesep, curr_name]));
            
            % if this is the first image, pre-initialize memory:
            if first_frame == 1;
                c1_img = zeros(size(curr_img,1), size(curr_img,2), max_planes, max_t);
                first_frame = 0;
            end
            % otherwise, you just save the image:
            c1_img(:,:, p_iter, t_iter) = curr_img;
        end
    end
    
    % now get each slice for the 2nd color
    txt_pre = 'r02c06f01';
    txt_end = 'fk1fl1.tiff';
    txt_color = '-ch1';
    % read the c1-channel
    first_frame = 1;
    for t_iter=1:max_t
        for p_iter = 1:max_planes
            % make the name:
            if p_iter < 10
                txt_plane = ['p0', num2str(p_iter)];
            elseif p_iter < 100
                txt_plane = ['p', num2str(p_iter)];
            end
            txt_time = ['sk', num2str(t_iter)];
            
            %define the current name:
            curr_name = [txt_pre, txt_plane, txt_color, txt_time, txt_end];
            curr_img = double(imread([MetaData.MovieData.Path, filesep, curr_name]));
            
            % if this is the first image, pre-initialize memory:
            if first_frame == 1;
                c2_img = zeros(size(curr_img,1), size(curr_img,2), max_planes, max_t);
                first_frame = 0;
            end
            % otherwise, you just save the image:
            c2_img(:,:, p_iter, t_iter) = curr_img;
        end
    end
    
   % finally add the frames to the FrameData structure/object:
   for iter = 1:size(c1_img,4)
       FrameData(iter,1).c1_img = c1_img(750:1350,400:1000,:,iter);
       FrameData(iter,1).c2_img = c2_img(750:1350,400:1000,:,iter);
   end
    
    toc;
end

end

