function [Points3D_c1, PSFfit_c1, img_3D_crop_c1, img_fit_3D_crop_c1, img_3D_crop_PSF_XYZ_c1, Points3D_c2, PSFfit_c2, img_3D_crop_c2, img_fit_3D_crop_c2, img_3D_crop_PSF_XYZ_c2] = ASH_Convert2ToCellArray(FrameData)
%ASH_CONVERT2TOCELLARRAY Convert to cell array
%   use the format desired by Jean-Yves Tinevez's 3D-Tracker

% initialize the cell arrays
TimeSteps = length(FrameData);
Points3D_c1 = cell(TimeSteps,1); Points3D_c2 = cell(TimeSteps,1); 
PSFfit_c1 = cell(TimeSteps,1); PSFfit_c2 = cell(TimeSteps,1); 
img_3D_crop_c1 = cell(TimeSteps,1); img_3D_crop_c2 = cell(TimeSteps,1); 
img_fit_3D_crop_c1 = cell(TimeSteps,1); img_fit_3D_crop_c2 = cell(TimeSteps,1); 
img_3D_crop_PSF_XYZ_c1 = cell(TimeSteps,1); img_3D_crop_PSF_XYZ_c2 = cell(TimeSteps,1); 
for iter = 1:TimeSteps
    % convert: 3 columns corresponding to XYZ coordinates in nm
    Points3D_c1{iter,1} = FrameData(iter,1).FinalDots_c1(:,4:6); 
    Points3D_c2{iter,1} = FrameData(iter,1).FinalDots_c2(:,4:6); 
    
    % now collect the other data about the points:
    % columns will be: sigma_x, sigma_y, sigma_z, amplitude, background
    PSFfit_c1{iter,1} = FrameData(iter,1).FinalDots_c1(:,[1:3, 7:8]);
    PSFfit_c2{iter,1} = FrameData(iter,1).FinalDots_c2(:,[1:3, 7:8]);
    
    % collect the 3D cropped images (some redundancy, but also a
    % lot of switching of indices, so just go with it):
    img_3D_crop_c1{iter,1} = FrameData(iter,1).c1_img_3D_crop;
    img_3D_crop_c2{iter,1} = FrameData(iter,1).c2_img_3D_crop;
    % also collect the 3D fit cropped images:
    img_fit_3D_crop_c1{iter,1} = FrameData(iter,1).c1_img_fit_3D_crop;
    img_fit_3D_crop_c2{iter,1} = FrameData(iter,1).c2_img_fit_3D_crop;
    
    % finally, collect the XYZ centers used for the 3D_img cropping:
    img_3D_crop_PSF_XYZ_c1{iter,1} = FrameData(iter,1).c1_img_3D_crop_PSF_XYZ;
    img_3D_crop_PSF_XYZ_c2{iter,1} = FrameData(iter,1).c2_img_3D_crop_PSF_XYZ;
end
% finish

end

