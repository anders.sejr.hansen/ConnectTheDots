function [ TrackedDotPairs ] = ASH_PLOT_QC_Save( MetaData, TrackedDotPairs, Step7Plots )
%ASH_PLOT_QC_SAVE QC, plot and save the data
%   The function will go through the following steps:
%       Step A: Calculate the signal-to-background for each loc
%       Step B: Plot the raw localization and the PSF-fit in XY, XZ and YZ
%               projections
%       Step C: Plot summary of each trajectory
%       Step D: Save a MAT-file with all the final localizations

%   Written by Anders Sejr Hansen (AndersSejrHansen@post.harvard.edu)
%   Dependent functions: 
%               ASH_Calc_Signal2Background.m
%               export_fig (https://github.com/altmany/export_fig)


tic;
%%%%%%%%% STEP A - CALCULATE THE SIGNAL-TO-BACKGROUND %%%%%%%%%%%%%%%%%%%%%
disp('Step 7.1: calculate the signal-to-background ratio for each localization');
% calculate the signal-to-background for each localization for each color
% in each trajectory:
TrackedDotPairs = ASH_Calc_Signal2Background(TrackedDotPairs);

% get ready to save the output images:
PlotPath = MetaData.SaveOptions.PlotPath;
PlotDir = 'Plots';
output_path = [PlotPath, filesep, PlotDir, filesep];
PlotPreName = MetaData.SaveOptions.PlotPreName;

%%%%%%%%% STEP B - Plot the raw image and the PSF-fit for each localization
disp('Step 7.2: Plot the raw localization image and the corresponding PSF-fit in XY, XZ and YZ');
%%%%%%%%%%% DEFINE AND PRE-DECLARE USEFUL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%
if Step7Plots == 1
    PixelSize = MetaData.PixelSize;
    zStep = MetaData.zStep;
    curr_img = TrackedDotPairs(1).c1.img_3D_crop{1}; % dummy img

    % Dimension of cropped image
    [dim.Y, dim.X, dim.Z] = size(curr_img);
    N_pix                 = dim.X*dim.Y*dim.Z;
    % generate a grid for the x-data (same for all the images)
    [Xs,Ys,Zs] = meshgrid(dim.Y:2*dim.Y-1,dim.X:2*dim.X-1,dim.Z:2*dim.Z-1);  % Pixel-grid has on offset from 0 to allow for negative values in center position
    xdata      = zeros(3,N_pix);
    xdata(1,:) = double(reshape(Xs,1,N_pix))*PixelSize;
    xdata(2,:) = double(reshape(Ys,1,N_pix))*PixelSize;
    xdata(3,:) = double(reshape(Zs,1,N_pix))*zStep;

    % Ranges for the plotting:
    XRange = [min(xdata(1,:)) max(xdata(1,:))];
    YRange = [min(xdata(2,:)) max(xdata(2,:))];
    ZRange = [min(xdata(3,:)) max(xdata(3,:))];
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %%%%%%%%%%%%%%% PLOT RAW DOT AND PSF FIT FOR EACH LOCALIZATION %%%%%%%%%%%%
    for PairIter = 1:length(TrackedDotPairs)

        PlotIter = 1; FirstPlot = 1;
        % Find and Plot localizations in this frame
        for FrameIter = 1:length(TrackedDotPairs(PairIter).Frames)
            % keep track of when to start a new figure;
            if PlotIter >= 4
                % reorganize the plot:
                %set(curr_fig,'Units','Inches');
                %pos = get(curr_fig,'Position');
                %set(curr_fig,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)]);
                % if this is the first iteration, plot a new figure
                if FirstPlot == 1;
                    export_fig(curr_fig,[output_path, PlotPreName, '_Pair', num2str(PairIter), '.pdf'],'-dpdf','-r0');
                else %if it's not the first run, append
                    export_fig(curr_fig,[output_path, PlotPreName, '_Pair', num2str(PairIter), '.pdf'],'-append', '-dpdf','-r0');
                end
                % now that you have finished one iteration, you can see it to
                % zero:
                FirstPlot = 0;            
                % make a new figure;
                PlotIter = 1;
                close;
            end
            if PlotIter == 1 % generate a new figure
                curr_fig = figure('position',[200 200 800 1200]); %[x y width height]
                set(gcf, 'color','w','Name',['ConnectTheDots: PSF-fits for PairedDots #', num2str(PairIter)]);
            end


            %%% CURRENT COORDINATES AND CROPPED IMAGES
            curr_img_c1 = TrackedDotPairs(PairIter).c1.img_3D_crop{FrameIter};
            curr_img_fit_c1 = TrackedDotPairs(PairIter).c1.img_fit_3D_crop{FrameIter};
            curr_img_3D_crop_PSF_XYZ_c1 = TrackedDotPairs(PairIter).c1.img_3D_crop_PSF_XYZ(FrameIter,:);
            curr_img_c2 = TrackedDotPairs(PairIter).c2.img_3D_crop{FrameIter};
            curr_img_fit_c2 = TrackedDotPairs(PairIter).c2.img_fit_3D_crop{FrameIter};
            curr_img_3D_crop_PSF_XYZ_c2 = TrackedDotPairs(PairIter).c2.img_3D_crop_PSF_XYZ(FrameIter,:);

            %%% Create XY, XZ, YZ Projections for both colors for the raw images:
            c1_MIP_xy = max(curr_img_c1,[],3);
            c1_MIP_xz = squeeze(max(curr_img_c1,[],1));
            c1_MIP_yz = squeeze(max(curr_img_c1,[],2)); 
            c2_MIP_xy = max(curr_img_c2,[],3);
            c2_MIP_xz = squeeze(max(curr_img_c2,[],1));
            c2_MIP_yz = squeeze(max(curr_img_c2,[],2)); 

            %%% Create XY, XZ, YZ Projections for both colors for the PSF fit images:
            c1_fit_MIP_xy = max(curr_img_fit_c1,[],3);
            c1_fit_MIP_xz = squeeze(max(curr_img_fit_c1,[],1));
            c1_fit_MIP_yz = squeeze(max(curr_img_fit_c1,[],2));
            c2_fit_MIP_xy = max(curr_img_fit_c2,[],3);
            c2_fit_MIP_xz = squeeze(max(curr_img_fit_c2,[],1));
            c2_fit_MIP_yz = squeeze(max(curr_img_fit_c2,[],2));

            %%% Get MIN and MAX of images for plotting:
            img_min_c1 = min(curr_img_c1(:)); img_max_c1 = max(curr_img_c1(:));
            img_min_c2 = min(curr_img_c2(:)); img_max_c2 = max(curr_img_c2(:));
            fit_min_c1 = min(curr_img_fit_c1(:)); fit_max_c1 = max(curr_img_fit_c1(:));
            fit_min_c2 = min(curr_img_fit_c2(:)); fit_max_c2 = max(curr_img_fit_c2(:));

             %%%%%%%%%%% PLOT COLOR 1 - RAW IMAGES AND FITTED CENTER %%%%%%%%%%%
            % Color 1 - XY 
            subplot(9,4,12*(PlotIter-1)+1);
            imshow(c1_MIP_xy,[img_min_c1 img_max_c1],'XData', XRange,'YData',YRange);
            title({'XY Max Int Project'; ['c1 PairedDot', num2str(PairIter), ' in Frame ', num2str(FrameIter)]},'FontWeight','Normal', 'FontName', 'Helvetica');    
            hold on; plot(curr_img_3D_crop_PSF_XYZ_c1(1), curr_img_3D_crop_PSF_XYZ_c1(2),'o', 'Color', [0/255; 153/255; 204/255]); hold off;

            % Color 1 - XZ
            subplot(9,4,12*(PlotIter-1)+5);
            imshow(c1_MIP_xz',[img_min_c1 img_max_c1],'XData', XRange,'YData',ZRange)
            title('XZ','FontWeight','Normal', 'FontName', 'Helvetica');
            colorbar
            hold on;  plot(curr_img_3D_crop_PSF_XYZ_c1(1), curr_img_3D_crop_PSF_XYZ_c1(3),'o', 'Color', [0/255; 153/255; 204/255] ); hold off;

            % Color 1 - YZ
            subplot(9,4,12*(PlotIter-1)+9);
            imshow(c1_MIP_yz',[img_min_c1 img_max_c1],'XData', YRange,'YData',ZRange)
            title('YZ','FontWeight','Normal', 'FontName', 'Helvetica');
            hold on; plot(curr_img_3D_crop_PSF_XYZ_c1(2), curr_img_3D_crop_PSF_XYZ_c1(3),'o', 'Color', [0/255; 153/255; 204/255]); hold off;

            %%%%%%%%%%% PLOT COLOR 1 - RAW IMAGES AND FITTED CENTER %%%%%%%%%%%
            % Color 1 - XY 
            subplot(9,4,12*(PlotIter-1)+2);
            imshow(c1_fit_MIP_xy,[fit_min_c1 fit_max_c1],'XData', XRange,'YData',YRange);
            title({'XY Max Int Project'; ['c2 PairedDot', num2str(PairIter), ' in Frame ', num2str(FrameIter)]},'FontWeight','Normal', 'FontName', 'Helvetica'); 
            hold on; plot(curr_img_3D_crop_PSF_XYZ_c1(1), curr_img_3D_crop_PSF_XYZ_c1(2),'o', 'Color', [0/255; 153/255; 204/255]); hold off;

            % Color 1 - XZ
            subplot(9,4,12*(PlotIter-1)+6);
            imshow(c1_fit_MIP_xz',[fit_min_c1 fit_max_c1],'XData', XRange,'YData',ZRange)
            title('XZ','FontWeight','Normal', 'FontName', 'Helvetica');
            colorbar
            hold on;  plot(curr_img_3D_crop_PSF_XYZ_c1(1), curr_img_3D_crop_PSF_XYZ_c1(3),'o', 'Color', [0/255; 153/255; 204/255] ); hold off;

            % Color 1 - YZ
            subplot(9,4,12*(PlotIter-1)+10);
            imshow(c1_fit_MIP_yz',[fit_min_c1 fit_max_c1],'XData', YRange,'YData',ZRange)
            title('YZ','FontWeight','Normal', 'FontName', 'Helvetica');
            hold on; plot(curr_img_3D_crop_PSF_XYZ_c1(2), curr_img_3D_crop_PSF_XYZ_c1(3),'o', 'Color', [0/255; 153/255; 204/255]); hold off;

            %%%%%%%%%%% PLOT COLOR 2 - RAW IMAGES AND FITTED CENTER %%%%%%%%%%%
            % Color 2 - XY 
            subplot(9,4,12*(PlotIter-1)+3);
            imshow(c2_MIP_xy,[img_min_c2 img_max_c2],'XData', XRange,'YData',YRange);
            title({'XY Max Int Project'; ['c2 PairedDot', num2str(PairIter), ' in Frame ', num2str(FrameIter)]},'FontWeight','Normal', 'FontName', 'Helvetica');    
            hold on; plot(curr_img_3D_crop_PSF_XYZ_c2(1), curr_img_3D_crop_PSF_XYZ_c2(2),'o', 'Color', [0/255; 153/255; 204/255]); hold off;

            % Color 2 - XZ
            subplot(9,4,12*(PlotIter-1)+7);
            imshow(c2_MIP_xz',[img_min_c2 img_max_c2],'XData', XRange,'YData',ZRange)
            title('XZ','FontWeight','Normal', 'FontName', 'Helvetica');
            colorbar
            hold on;  plot(curr_img_3D_crop_PSF_XYZ_c2(1), curr_img_3D_crop_PSF_XYZ_c2(3),'o', 'Color', [0/255; 153/255; 204/255] ); hold off;

            % Color 2 - YZ
            subplot(9,4,12*(PlotIter-1)+11);
            imshow(c2_MIP_yz',[img_min_c2 img_max_c2],'XData', YRange,'YData',ZRange)
            title('YZ','FontWeight','Normal', 'FontName', 'Helvetica');
            hold on; plot(curr_img_3D_crop_PSF_XYZ_c2(2), curr_img_3D_crop_PSF_XYZ_c2(3),'o', 'Color', [0/255; 153/255; 204/255]); hold off;

            %%%%%%%%%%% PLOT COLOR 2 - RAW IMAGES AND FITTED CENTER %%%%%%%%%%%
            % Color 2 - XY 
            subplot(9,4,12*(PlotIter-1)+4);
            imshow(c2_fit_MIP_xy,[fit_min_c2 fit_max_c2],'XData', XRange,'YData',YRange);
            title({'XY Max Int Project'; ['c2 PairedDot', num2str(PairIter), ' in Frame ', num2str(FrameIter)]},'FontWeight','Normal', 'FontName', 'Helvetica'); 
            hold on; plot(curr_img_3D_crop_PSF_XYZ_c2(1), curr_img_3D_crop_PSF_XYZ_c2(2),'o', 'Color', [0/255; 153/255; 204/255]); hold off;

            % Color 2 - XZ
            subplot(9,4,12*(PlotIter-1)+8);
            imshow(c2_fit_MIP_xz',[fit_min_c2 fit_max_c2],'XData', XRange,'YData',ZRange)
            title('XZ','FontWeight','Normal', 'FontName', 'Helvetica');
            colorbar
            hold on;  plot(curr_img_3D_crop_PSF_XYZ_c2(1), curr_img_3D_crop_PSF_XYZ_c2(3),'o', 'Color', [0/255; 153/255; 204/255] ); hold off;

            % Color 2 - YZ
            subplot(9,4,12*(PlotIter-1)+12);
            imshow(c2_fit_MIP_yz',[fit_min_c2 fit_max_c2],'XData', YRange,'YData',ZRange)
            title('YZ','FontWeight','Normal', 'FontName', 'Helvetica');
            hold on; plot(curr_img_3D_crop_PSF_XYZ_c2(2), curr_img_3D_crop_PSF_XYZ_c2(3),'o', 'Color', [0/255; 153/255; 204/255]); hold off;

            % finished this round:
            PlotIter = PlotIter + 1;

        end 
        % finally save the last figure if it was not saved already:
        if FirstPlot == 1;
            export_fig(curr_fig,[output_path, PlotPreName, '_Pair', num2str(PairIter), '.pdf'],'-dpdf','-r0');
        elseif  PlotIter < 4 %if it's not the first run, append
            export_fig(curr_fig,[output_path, PlotPreName, '_Pair', num2str(PairIter), '.pdf'],'-append', '-dpdf','-r0');
        end
        close;

    end
end

%%%%%%%%% STEP C - Plot summary of each trajectory %%%%%%%%%%%%%%%%%%%%%%%%
disp('Step 7.3: Plot summary of each MatchedPair trajectory (3D-dist and S/B)');
PlotIter = 1; NumPlot = 1;
for PairIter = 1:length(TrackedDotPairs)
    % get the key info
    curr_time = TrackedDotPairs(PairIter,1).TimeStamp;
    curr_Dist3D = TrackedDotPairs(PairIter,1).Dist3D;
    curr_S2B = TrackedDotPairs(PairIter,1).c1c2_Signal2Background;
    if PlotIter >= 13            
        if NumPlot == 1
            export_fig(curr_fig,[output_path, PlotPreName, '_OverallSummary.pdf'],'-dpdf','-r0');
        else
            export_fig(curr_fig,[output_path, PlotPreName, '_OverallSummary.pdf'],'-append', '-dpdf','-r0');
        end
        NumPlot = NumPlot + 1;
        PlotIter = 1;
    end
    % first plot? 
    if PlotIter == 1 % generate a new figure
        curr_fig = figure('position',[100 100 900 1200]); %[x y width height]
        set(gcf, 'color','w','Name','ConnectTheDots: Summary plots');
    end
    
    % Plot 3D distance
    subaxis(6,4,2*(PlotIter-1)+1);
    hold on;
    plot(curr_time, curr_Dist3D, 'o', 'MarkerSize', 4, 'Color',  [0/255, 153/255, 204/255]);
    plot(curr_time, curr_Dist3D, '-', 'LineWidth', 1, 'Color',  [0/255, 153/255, 204/255]);
    axis([0 1.05*max(curr_time) 0 1200]);
    title(['MatchedPair #', num2str(PairIter), ': 3D dist vs. time'],'FontWeight','Normal','FontSize',9);
    xlabel('time (min)', 'FontSize',8, 'FontName', 'Helvetica', 'Color', 'k');
    ylabel('3D distance (nm)', 'FontSize',8, 'FontName', 'Helvetica', 'Color', 'k');
    hold off;
    
    % Plot Signal-to-Background
    subaxis(6,4,2*(PlotIter-1)+2);
    hold on;
    plot(curr_time, curr_S2B(:,1), 'o', 'MarkerSize', 4, 'Color',  [0/255, 161/255, 75/255]);
    plot(curr_time, curr_S2B(:,2), 'o', 'MarkerSize', 4, 'Color',  [237/255, 28/255, 36/255]);
    plot(curr_time, curr_S2B(:,2), '-', 'LineWidth', 1, 'Color',  [237/255, 28/255, 36/255]);
    plot(curr_time, curr_S2B(:,1), '-', 'LineWidth', 1, 'Color',  [0/255, 161/255, 75/255]);
    plot([0; 1.05*max(curr_time)], [1; 1], 'k--', 'LineWidth', 1);
    axis([0 1.05*max(curr_time) 0 max([120 max(max(curr_S2B))])]);
    title(['MatchedPair #', num2str(PairIter), ': Signal/Background'],'FontWeight','Normal','FontSize',9);
    xlabel('time (min)', 'FontSize',8, 'FontName', 'Helvetica', 'Color', 'k');
    ylabel('signal/background', 'FontSize',8, 'FontName', 'Helvetica', 'Color', 'k');
    legend('c1', 'c2', 'Location', 'NorthEast');
    legend boxoff
    hold off;
    
    % finish this round
    PlotIter = PlotIter + 1;
end
% finally save the last figure if it was not saved already:
if NumPlot == 1;
    export_fig(curr_fig,[output_path, PlotPreName, '_OverallSummary.pdf'],'-dpdf','-r0');
elseif  PlotIter < 13 %if it's not the first run, append
    export_fig(curr_fig,[output_path, PlotPreName, '_OverallSummary.pdf'],'-append', '-dpdf','-r0');
end
close;


%%%%%%%%%%%%%%%%%% STEP D - SAVE A MAT-FILE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp(['Step 7.4: Save a MAT-file with ', num2str(length(TrackedDotPairs)), ' trajectories']);
disp(['saving to ', MetaData.SaveOptions.SaveMATpath, '...']);
% make sure the folder for saving exists:
% make a folder for saving the tracking plots:
output_path = MetaData.SaveOptions.SaveMATpath;
if exist(output_path, 'dir');
    % OK, output_path exists and is a directory (== 7). 
else
    mkdir(output_path);
    disp('The given output folder did not exist, but was just created.');
    disp(output_path);
end
% save the following variables/structured objects:
%   - MetaData: lot's of relevant info
%   - TrackedPairDots: key info for each trajectory
save([output_path, MetaData.MovieData.c1_name,'_TrackedPairs.mat'], 'MetaData', 'TrackedDotPairs');


toc;


end

