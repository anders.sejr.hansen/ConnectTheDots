function [AllTracks, LongTracks] = ASH_3D_track(Points3D, PSFfit, img_3D_crop_cell, img_fit_3D_crop_cell, img_3D_crop_PSF_XYZ_cell, MaxGaps, MinLocFrac, TrackMethod, max_link_dist, TimeStep)
%ASH_3D_TRACK Track dots in 3D
%  This function requires Jean-Yves Tinevez's SimpleTracker matlab library
%  which has 2 methods implemented: 'Hungarian', 'NearestNeighbor'

%   Written by Anders Sejr Hansen

% Perform 3D tracking:
tracks = simpletracker(Points3D, 'MaxLinkingDistance',max_link_dist,'MaxGapClosing',MaxGaps, 'Method', TrackMethod);

% store trajectories as structured arrays:
AllTracks = struct(); LongTracks = struct();

% "tracks" will be a cell array, where each element is columnvector of size
% Nx1, where N is the number of timepoints. Each element of the vector is
% an integer corresponding to the index of the tracked particle.
% For convenience convert to a structured array format where each element
% is named:
%   XYZ: XYZ coordinates in nm
%   PSF_sigma_XYZ_nm: size of sigma for the Gaussian PSF fit
%   Amplitude: signal in AU
%   Background: in AU
%   also saved the 3D cropped img around each detection

% define a threshold length for the long tracks:
minTrackLength = round( MinLocFrac * length(Points3D) + 1);
if minTrackLength > length(Points3D) % make sure threshold does not exceed the number of frames
    minTrackLength = length(Points3D);
end


% loop over all tracks:
long_iter = 1;
for track_iter = 1:length(tracks)
    all_idx = tracks{track_iter};
    all_frames = (1:length(all_idx))';
    % remove the NaNs:
    curr_idx = all_idx(~isnan(all_idx));
    curr_frames = all_frames(~isnan(all_idx));
    N = length(curr_idx);
    % declare all the desired variables:
    XYZ = zeros(N,3);
    PSF_sigma_XYZ_nm = zeros(N,3);
    Amplitude = zeros(N,1);
    Background = zeros(N,1);
    % 3D cropped image (raw and fitted) should be a cell array:
    img_3D_crop = cell(N,1);
    img_fit_3D_crop = cell(N,1);
    img_3D_crop_PSF_XYZ = zeros(N,3);
    % loop over all the frames where the dot was tracked
    for iter = 1:N
        idx = curr_idx(iter);
        curr_t = curr_frames(iter);
        % now extract the relevant info:
        XYZ(iter,:) = Points3D{curr_t}(idx,:);
        PSF_sigma_XYZ_nm(iter,:) = PSFfit{curr_t}(idx,1:3);
        Amplitude(iter,:) = PSFfit{curr_t}(idx,4);
        Background(iter,:) = PSFfit{curr_t}(idx,5);
        img_3D_crop{iter,1} = img_3D_crop_cell{curr_t}{idx};
        img_fit_3D_crop{iter,1} = img_fit_3D_crop_cell{curr_t}{idx};
        img_3D_crop_PSF_XYZ(iter,:) = img_3D_crop_PSF_XYZ_cell{curr_t}(idx,:);
    end
    % now save all the data to the track
    AllTracks(track_iter,1).XYZ = XYZ;
    AllTracks(track_iter,1).Frames = curr_frames;
    AllTracks(track_iter,1).TimeStamp = TimeStep*(curr_frames-1);
    AllTracks(track_iter,1).PSF_sigma_XYZ_nm = PSF_sigma_XYZ_nm;
    AllTracks(track_iter,1).Amplitude = Amplitude;
    AllTracks(track_iter,1).Background = Background;
    AllTracks(track_iter,1).img_3D_crop = img_3D_crop;
    AllTracks(track_iter,1).img_fit_3D_crop = img_fit_3D_crop;
    AllTracks(track_iter,1).img_3D_crop_PSF_XYZ = img_3D_crop_PSF_XYZ;
    
    % does this track qualify as a long track:
    if size(XYZ,1) >= minTrackLength
        % if it is long enough, then save all the stuff:
        LongTracks(long_iter,1).XYZ = XYZ;
        LongTracks(long_iter,1).Frames = curr_frames;
        LongTracks(long_iter,1).TimeStamp = TimeStep*(curr_frames-1);
        LongTracks(long_iter,1).PSF_sigma_XYZ_nm = PSF_sigma_XYZ_nm;
        LongTracks(long_iter,1).Amplitude = Amplitude;
        LongTracks(long_iter,1).Background = Background;
        LongTracks(long_iter,1).img_3D_crop = img_3D_crop;
        LongTracks(long_iter,1).img_fit_3D_crop = img_fit_3D_crop;
        LongTracks(long_iter,1).img_3D_crop_PSF_XYZ = img_3D_crop_PSF_XYZ;
        % finally update the counter:
        long_iter = long_iter + 1;
    end
end



end

