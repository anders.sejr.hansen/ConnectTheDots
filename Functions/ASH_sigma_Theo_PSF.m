function [sigma_xy,sigma_z] = ASH_sigma_Theo_PSF(lambda_ex, lambda_em, NA, RI, microscope)
% Compute the sigmas of the bi-Gaussian approximation of the PSF
% Using equations of Zhang, B., Zerubia, J. and Olivo-Marin, J.C., 2007. 
% Gaussian approximations of fluorescence microscope point-spread function models. 
% Applied optics, 46(10), pp.1819-1829.



% lambda_em = emission wavelength
% lambda_ex = excitation wavelength
% NA = numerical aperture of the objective lens
% n = refractive index of the sample medium
% microscope = string that determines microscope type: 'widefield',
% 'confocal' or 'nipkow'


if isempty(lambda_ex)
    lambda_ex = lambda_em;
end

switch microscope
    case 'widefield'    % Widefield Microscope
        sigma_xy = 0.225 * lambda_em / NA ;
        sigma_z  = 0.78 * RI * lambda_em / (NA*NA) ;

    case {'confocal', 'nipkow'}   % Laser Scanning Confocal Microscope and Spinning Disc Confocal Microscope
        sigma_xy = 0.225 / NA * lambda_ex * lambda_em / sqrt( lambda_ex^2 + lambda_em^2 ) ; % equation #16 in Zhang, B., Zerubia, J. and Olivo-Marin, J.C., 2007. Gaussian approximations of fluorescence microscope point-spread function models. Applied optics, 46(10), pp.1819-1829.
        sigma_z =  0.78 * RI / (NA^2) *  lambda_ex * lambda_em / sqrt( lambda_ex^2 + lambda_em^2 ) ;

    otherwise
        error(['microscope = ',microscope,' is not a valid option !']);
end

