function [DetectedDots_XYZ] = ASH_ThresholdDots(img_filt, method, threshold, xy_detect, z_detect, PSF_theo_xy_pix, PSF_theo_z_pix, PSF_xy_padding, PSF_z_padding)
%ASH_THRESHOLDDOTS Locate dots in 3D using thresholding
%   This script takes as input a filtered image (contrast enhanced), a
%   method (either 'nonMaxSuppression' or 'ConnectedComponents', a
%   threshold and a detection box and then returns a Nx3 matrix with the
%   XYZ coordinates of the Detected/Thresholded dots

%   Written by Anders Sejr Hansen
%   Dependent functions:               
%               nonMaxSupr.m
%               nlfiltersep.m
%               ind2sub2.m
%   Connected Components method based on the Logic implemented by Florian
%   Mueller and Herve Marie-Nelly in FISH-Quant

remove_dots_that_are_too_close_to_each_other = 0; % generally this seems to cause more problems

% Get dimension of the image:
[dim.Y, dim.X, dim.Z] = size(img_filt);

if strcmp(method, 'nonMaxSuppression')
    % LOCATE THE DOTS USING NON-MAX SUPPRESION
    thresh_int  = prctile(img_filt(:),threshold); % set a treshold
    
    % Define filter:
    rad_detect = [xy_detect xy_detect z_detect];

    if remove_dots_that_are_too_close_to_each_other == 1
        % Pre-detection:
        positions = nonMaxSupr(double(img_filt), rad_detect, thresh_int);

        % Remove dots that are within half the detection radius
        %  This can occur when small pixels are used
        positions_sort = sortrows(positions);

        positions_diff = abs(diff(positions_sort));

        ind_x_0 = positions_diff(:,2) <= ceil(xy_detect);
        ind_y_0 = positions_diff(:,1) <= ceil(xy_detect);
        ind_z_0 = positions_diff(:,3) <= ceil(z_detect);

        positions_diff(ind_x_0,2) = 0;
        positions_diff(ind_y_0,1) = 0;
        positions_diff(ind_z_0,3) = 0;

        ind_remove = ismember(positions_diff,[0 0 0],'rows');


        positions_predetect = positions_sort;
        positions_predetect(ind_remove,:) = [];
        
    else
        % Pre-detection:
        positions_predetect = nonMaxSupr(double(img_filt), rad_detect, thresh_int);
    end
        
    

    % this gives the thresholded positions in a way where positions_predetect
    % contains the X,Y,Z coordinates of the tresholded dots (really it's
    % the YXZ coordinates)

elseif strcmp(method, 'ConnectedComponents')
    % LOCATE THE DOTS USING Connected Components
    % Uses the Matlab function (part of image processing toolbox)
    thresh_int  = prctile(img_filt(:),threshold); % set a treshold
    
    connectivity_3D = 26;   % Connectivity in 3D, default is 26
    % Apply threshold
    bwl = img_filt > thresh_int & img_filt;

    % Find particles
    CC = bwconncomp(bwl,connectivity_3D);

    % Get centroid of each identified region
            % Old version:
            %CC_best = CC{1};
            %S = regionprops(CC_best,'Centroid');
    S = regionprops(CC,'Centroid');
    N_spots = CC.NumObjects;
    centroid_linear  = [S.Centroid]';

    % some of the spots will be connected:
    centroid_matrix      = round(reshape(centroid_linear,3,N_spots))'; % 3D version
    % get the centroid coordinates
    positions_predetect(:,1) = centroid_matrix(:,2);
    positions_predetect(:,2) = centroid_matrix(:,1);
    positions_predetect(:,3) = centroid_matrix(:,3);
    

else
    error(['error: the thresholding method you supplied, ', method, ', is not supported. Use "nonMaxSuppression" or "ConnectedComponents" instead']);
end

% Later on you will need to do least-squares fine-scale PSF fitting and
% here you need to make sure that the spots are not too close to the edges.
% Use the shape of the theoretical PSF with some extra padding (pixel
% units) to ensure that no errors occur:
xy_padding = round(2*PSF_theo_xy_pix)+PSF_xy_padding; z_padding = round(2*PSF_theo_z_pix)+PSF_z_padding;
ind_x   = (positions_predetect(:,1) > xy_padding) & (positions_predetect(:,1) <= dim.Y-xy_padding);
ind_y   = (positions_predetect(:,2) > xy_padding) & (positions_predetect(:,2) <= dim.X-xy_padding);
ind_z   = (positions_predetect(:,3) > z_padding)  & (positions_predetect(:,3) <= dim.Z-z_padding);    
GoodDots = ind_x & ind_y & ind_z; % the ones that are not too close to the edges

% All done, now return the good detected dots:
DetectedDots_XYZ = positions_predetect(GoodDots,:);

end
