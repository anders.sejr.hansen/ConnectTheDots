function [FrameData, MetaData] = ASH_LocateTheDots(FrameData, MetaData, CurrStep)
%ASH_LOCATETHEDOTS Find the rough position of the dots
%   This function goes through the following steps:
%       Step A: generate the 3D PSF
%       Step B: threshold the zStacks and find dots
%       Step C: Extra QC: make sure spots resemble PSFs
%       Step D: Plot the success of the current detection parameters

%   Written by Anders Sejr Hansen
%   Dependent functions: 
%               ASH_sigma_Theo_PSF.m
%               ASH_ThresholdDots.m
%               nonMaxSupr.m
%               nlfiltersep.m
%               ind2sub2.m
%               subaxis.m

tic;
% check to see if you are just adjusting the thresholds (CurrStep=1) or
% if you are analyzing all of the data (CurrStep=2); 
if CurrStep == 1 % only first and last
    FramesToAnalyze = [1, length(FrameData)]; % first and last frames only
elseif CurrStep == 2 % do all frames
    FramesToAnalyze = 1:length(FrameData);
end





%%%%%%%%%% STEP A - GENERATE THE 3D PSFs FOR BOTH COLORS %%%%%%%%%%%%%%%%%%
disp('Step 3.1: generate the theoretical 3D PSFs');
[c1_PSF_theo_xy_nm,c1_PSF_theo_z_nm] = ASH_sigma_Theo_PSF(MetaData.c1Excitation, MetaData.c1Emission, ...
                                MetaData.NA, MetaData.RefractiveIndex, MetaData.MicroscopeType);
[c2_PSF_theo_xy_nm,c2_PSF_theo_z_nm] = ASH_sigma_Theo_PSF(MetaData.c2Excitation, MetaData.c2Emission, ...
                                MetaData.NA, MetaData.RefractiveIndex, MetaData.MicroscopeType);
% Need this in both nm and pixel units:
c1_PSF_theo_xy_pix = c1_PSF_theo_xy_nm / MetaData.PixelSize; c2_PSF_theo_xy_pix = c2_PSF_theo_xy_nm / MetaData.PixelSize; 
c1_PSF_theo_z_pix = c1_PSF_theo_z_nm / MetaData.zStep; c2_PSF_theo_z_pix = c2_PSF_theo_z_nm / MetaData.zStep; 
% also save the Theoretical PSFs to the MetaData structure/object
MetaData.c1_PSF_theo_xy_nm = c1_PSF_theo_xy_nm; MetaData.c2_PSF_theo_xy_nm = c2_PSF_theo_xy_nm;
MetaData.c1_PSF_theo_z_nm = c1_PSF_theo_z_nm; MetaData.c2_PSF_theo_z_nm = c2_PSF_theo_z_nm;
MetaData.c1_PSF_theo_xy_pix = c1_PSF_theo_xy_pix; MetaData.c2_PSF_theo_xy_pix = c2_PSF_theo_xy_pix;
MetaData.c1_PSF_theo_z_pix = c1_PSF_theo_z_pix; MetaData.c2_PSF_theo_z_pix = c2_PSF_theo_z_pix;






%%%%%%%%%% STEP B - LOCATE THE ROUGH POSITION OF THE DOTS %%%%%%%%%%%%%%%%%
% for detection you will need 3 key parameters:
%   1. Method (either 'nonMaxSuppression' or 'ConnectedComponents')
%   2. The threshold (in percentile, e.g. 99.9)
%   3. The XY,Z padding (NB. expect false negatives if spots close than this)
disp('Step 3.2: threshold and locate dots using the filtered images');
c1_method = MetaData.DectectionOptions.c1_method; c2_method = MetaData.DectectionOptions.c2_method;
c1_threshold = MetaData.DectectionOptions.c1_threshold; c2_threshold = MetaData.DectectionOptions.c2_threshold;

% for the padding used for detectection, add the extra padding from 
% "MetaData.DectectionOptions" to the rounded theoretical PSF:
c1_xy_detect =  round(2*c1_PSF_theo_xy_pix)+MetaData.DectectionOptions.c1_xy_padding;
c2_xy_detect =  round(2*c2_PSF_theo_xy_pix)+MetaData.DectectionOptions.c2_xy_padding;
c1_z_detect =  round(2*c1_PSF_theo_z_pix)+MetaData.DectectionOptions.c1_z_padding;
c2_z_detect =  round(2*c2_PSF_theo_z_pix)+MetaData.DectectionOptions.c2_z_padding;

% Now do the actual thresholding:
for iter = 1:length(FramesToAnalyze)
    CurrFrame = FramesToAnalyze(iter);
    
    % threshold the filtered images
    [FrameData(CurrFrame,1).DetectedDots_XYZ_c1] = ASH_ThresholdDots(FrameData(CurrFrame,1).c1_img_filt, c1_method, c1_threshold, c1_xy_detect, c1_z_detect, c1_PSF_theo_xy_pix, c1_PSF_theo_z_pix, MetaData.PSF_xy_padding, MetaData.PSF_z_padding);
    [FrameData(CurrFrame,1).DetectedDots_XYZ_c2] = ASH_ThresholdDots(FrameData(CurrFrame,1).c2_img_filt, c2_method, c2_threshold, c2_xy_detect, c2_z_detect, c2_PSF_theo_xy_pix, c2_PSF_theo_z_pix, MetaData.PSF_xy_padding, MetaData.PSF_z_padding);
end

%%%%%%%%%% STEP C - ADDITIONAL QC: Make sure it resembles a PSF %%%%%%%%%%%
% if the dot resembles a PSF, it should be much, much brighter in the
% center than around the edges. So one way to test this using the raw
% images instead of the LoG enhanced image, is to compare the intensity of
% the perimeter of a circle with the intensity of the center dot. 
% For example, use a ring 6 pixels away from PSF center in the central
% Z-position and plus/minus 1 above and below. 
disp('Step 3.3: perform additional QC on detected dots using a ring-algorithm');
% Now do the actual thresholding:
for iter = 1:length(FramesToAnalyze)
    CurrFrame = FramesToAnalyze(iter);
    [FrameData(CurrFrame,1).QC_Dots_XYZ_c1] = ASH_RingQC_Dots(FrameData(CurrFrame,1).c1_img, FrameData(CurrFrame,1).DetectedDots_XYZ_c1, ...
                                                MetaData.DectectionOptions.c1_RingRadius, MetaData.DectectionOptions.c1_QC_thres);
    [FrameData(CurrFrame,1).QC_Dots_XYZ_c2] = ASH_RingQC_Dots(FrameData(CurrFrame,1).c2_img, FrameData(CurrFrame,1).DetectedDots_XYZ_c2, ...
                                                MetaData.DectectionOptions.c2_RingRadius, MetaData.DectectionOptions.c2_QC_thres);
end
toc;











%%%%%%%%%% STEP D - PLOT THE DOT DETECTION IF CurrStep=1 %%%%%%%%%%%%%%%%%%
if CurrStep == 1
    disp('Step 3.4: plot the currently detected dots overlaid on max-Z projected images'); tic;
    % If CurrStep = 1, then the goal is determining the optimal thresholds and
    % dectection ranges and therefore you should now plot an image of the
    % detection so the user can assess whether or not parameters need to
    % change:

    % Plot using sub-axis
    figure('position',[100 100 1600 800]); %[x y width height]
    set(gcf,'Name','ConnectTheDots: optimize thresholding parameters');
    
    % generate a gray colormap:
    GrayColorMap = zeros(64,3);
    GreenColorMap = zeros(64,3);
    MagentaColorMap = zeros(64,3);
    for iter=1:size(GrayColorMap,1)
        GrayColorMap(iter,:) = ((iter-1)/(size(GrayColorMap,1)-1)) .* [1; 1; 1;];
        GreenColorMap(iter,:) = ((iter-1)/(size(GreenColorMap,1)-1)) .* [0; 1; 0;];
        MagentaColorMap(iter,:) = ((iter-1)/(size(MagentaColorMap,1)-1)) .* [1; 0; 1;];
    end
    
    
    % temp store contrast adjusted images in a cell array:
    c1_cell = cell(1,length(FramesToAnalyze));
    c2_cell = cell(1,length(FramesToAnalyze));
    
    % perform the plotting
    for iter = 1:length(FramesToAnalyze)
        CurrFrame = FramesToAnalyze(iter);
        
        % process the images to make sure the contrast is right
        % max-Z projections:
        curr_c1 = max(FrameData(CurrFrame,1).c1_img, [], 3);
        curr_c1_filt = max(FrameData(CurrFrame,1).c1_img_filt, [], 3);
        curr_c2 = max(FrameData(CurrFrame,1).c2_img, [], 3);
        curr_c2_filt = max(FrameData(CurrFrame,1).c2_img_filt, [], 3);
        
        % subtract the background:
        curr_c1 = curr_c1 - 0.95*min(curr_c1(:));
        curr_c1_filt = curr_c1_filt - 0.95*min(curr_c1_filt(:));
        curr_c2 = curr_c2 - 0.95*min(curr_c2(:));
        curr_c2_filt = curr_c2_filt - 0.95*min(curr_c2_filt(:));
        
        % normalize by 1.5x 99.99 percentile and convert to 8bit:
        Max_c1 = prctile(curr_c1(:), 99.999);
        norm_c1 = curr_c1/(2*Max_c1);
        norm_c1(norm_c1>1) = 1;    
        c1_uint8 = uint8(floor(norm_c1 * 255)); % convert to 8bit
        c1_uint8_gray = ind2rgb(c1_uint8, GrayColorMap); % convert to rgb:
        % for filt
        Max_c1_filt = prctile(curr_c1_filt(:), 99.999);
        norm_c1_filt = curr_c1_filt/(1.3*Max_c1_filt);
        norm_c1_filt(norm_c1_filt>1) = 1;    
        c1_uint8_filt = uint8(floor(norm_c1_filt * 255)); % convert to 8bit
        c1_uint8_filt_gray = ind2rgb(c1_uint8_filt, GrayColorMap); % convert to rgb:
        % repeat for color 2:        
        Max_c2 = prctile(curr_c2(:), 99.999);
        norm_c2 = curr_c2/(1.3*Max_c2);
        norm_c2(norm_c2>1) = 1;    
        c2_uint8 = uint8(floor(norm_c2 * 255)); % convert to 8bit
        c2_uint8_gray = ind2rgb(c2_uint8, GrayColorMap); % convert to rgb:
        % for filt
        Max_c2_filt = prctile(curr_c2_filt(:), 99.999);
        norm_c2_filt = curr_c2_filt/(1.3*Max_c2_filt);
        norm_c2_filt(norm_c2_filt>1) = 1;    
        c2_uint8_filt = uint8(floor(norm_c2_filt * 255)); % convert to 8bit
        c2_uint8_filt_gray = ind2rgb(c2_uint8_filt, GrayColorMap); % convert to rgb:
        
        % store in cell array:
        c1_cell{1,iter} = c1_uint8; c2_cell{1,iter} = c2_uint8;
        
        %%%%%%% SUB-PLOT 1+n: pre-detection in the first frame (color 1):
        h1 = subaxis(2,4,4*(iter-1)+1);
        image(c1_uint8_gray); hold on;
        plot(FrameData(CurrFrame,1).DetectedDots_XYZ_c1(:,2),FrameData(CurrFrame,1).DetectedDots_XYZ_c1(:,1),'gx','MarkerSize',3);
        hold off;
        title({['c1 frame ', num2str(CurrFrame), ': detections (raw)']; ['LoG r: ', num2str(MetaData.FilterOptions.c1_radius), ...
                '; LoG sigma: ', num2str(MetaData.FilterOptions.c1_sigma), '; PrcTile thres: ', num2str(MetaData.DectectionOptions.c1_threshold)]; ...
                ['Method: ', MetaData.DectectionOptions.c1_method];},'FontWeight','Normal');
        set(gca,'XTickLabel',[]); set(gca,'YTickLabel',[]);
        
        %%%%%%% SUB-PLOT 1+n: pre-detection in the first frame (color 1):
        h2 = subaxis(2,4,4*(iter-1)+2);
        %imshow(imadjust(max(FrameData(CurrFrame,1).c1_img_filt, [], 3), [0.6; 1],[0; 1]),[]); hold on;
        %imshow(uint8(max(FrameData(CurrFrame,1).c1_img_filt, [], 3)),[]); hold on;
        image(c1_uint8_filt_gray); hold on;
        plot(FrameData(CurrFrame,1).DetectedDots_XYZ_c1(:,2),FrameData(CurrFrame,1).DetectedDots_XYZ_c1(:,1),'gx','MarkerSize',3);
        plot(FrameData(CurrFrame,1).QC_Dots_XYZ_c1(:,2),FrameData(CurrFrame,1).QC_Dots_XYZ_c1(:,1),'co','MarkerSize',10);

        hold off;
        title({['c1 frame ', num2str(CurrFrame), ': detections (contrast enhanced)']; ...
            ['QC Ring r: ', num2str(MetaData.DectectionOptions.c1_RingRadius), ' (pix) ; QC Dot fold-int: ', num2str(MetaData.DectectionOptions.c1_QC_thres) ]; ...
            ['LoG ',num2str(size(FrameData(CurrFrame,1).DetectedDots_XYZ_c1,1)),' dots (green); ring-QC: ',num2str(size(FrameData(CurrFrame,1).QC_Dots_XYZ_c1,1)),' dots (cyan)']},'FontWeight','Normal');
        set(gca,'XTickLabel',[]); set(gca,'YTickLabel',[]);
        linkaxes([h1,h2], 'xy');
        
        %%%%%%% SUB-PLOT 1+n: pre-detection in the first frame (color 2):
        h3 = subaxis(2,4,4*(iter-1)+3);
        %imshow(max(FrameData(CurrFrame,1).c2_img, [], 3),[]); hold on;
        image(c2_uint8_gray); hold on;
        plot(FrameData(CurrFrame,1).DetectedDots_XYZ_c2(:,2),FrameData(CurrFrame,1).DetectedDots_XYZ_c2(:,1),'gx','MarkerSize',3)
        hold off;
        title({['c2 frame ', num2str(CurrFrame), ': detections (raw)']; ['LoG r: ', num2str(MetaData.FilterOptions.c2_radius), ...
                '; LoG sigma: ', num2str(MetaData.FilterOptions.c2_sigma), '; PrcTile thres: ', num2str(MetaData.DectectionOptions.c2_threshold)];...
                ['Method: ', MetaData.DectectionOptions.c2_method];},'FontWeight','Normal');
        set(gca,'XTickLabel',[]); set(gca,'YTickLabel',[]);
            
        %%%%%%% SUB-PLOT 1+n: pre-detection in the first frame (color 2):
        h4 = subaxis(2,4,4*(iter-1)+4);
        %imshow(imadjust(max(FrameData(CurrFrame,1).c2_img_filt, [], 3), [0.2; 1],[0; 1]),[]); hold on;
        %imshow(uint8(max(FrameData(CurrFrame,1).c2_img_filt, [], 3)),[]); hold on;
        image(c2_uint8_filt_gray); hold on;
        plot(FrameData(CurrFrame,1).DetectedDots_XYZ_c2(:,2),FrameData(CurrFrame,1).DetectedDots_XYZ_c2(:,1),'gx','MarkerSize',3);
        plot(FrameData(CurrFrame,1).QC_Dots_XYZ_c2(:,2),FrameData(CurrFrame,1).QC_Dots_XYZ_c2(:,1),'co','MarkerSize',10);
        hold off;
        title({['c2 frame ', num2str(CurrFrame), ': detections (contrast enhanced)']; ...
            ['QC Ring r: ', num2str(MetaData.DectectionOptions.c2_RingRadius), ' (pix) ; QC Dot fold-int: ', num2str(MetaData.DectectionOptions.c2_QC_thres) ]; ...
            ['LoG ',num2str(size(FrameData(CurrFrame,1).DetectedDots_XYZ_c2,1)),' dots (green); ring-QC: ',num2str(size(FrameData(CurrFrame,1).QC_Dots_XYZ_c2,1)),' dots (cyan)']},'FontWeight','Normal');
        set(gca,'XTickLabel',[]); set(gca,'YTickLabel',[]);
        linkaxes([h3,h4], 'xy');
    end
    
    % finally display large images of a MaxZ projections to avoid all the
    % labels complicating things:
    
    for iter = 1:length(FramesToAnalyze)
        CurrFrame = FramesToAnalyze(iter);
        % Plot green MaxZ
        figure('position',[100 100 900 800]); %[x y width height]
        set(gcf,'Name','ConnectTheDots: Raw MaxZ of color 1');
        rgb_c1 = ind2rgb(c1_cell{1,iter}, GreenColorMap); % convert to rgb:
        image(rgb_c1); hold on;
        set(gca,'XTickLabel',[]); set(gca,'YTickLabel',[]);
        title(['MaxZ of color 1 at time ', num2str(MetaData.MovieData.TimeStep * (CurrFrame-1)), ' min'],'FontWeight','Normal');
        hold off;

        % Plot Magenta MaxZ
        figure('position',[100 200 900 800]); %[x y width height]
        set(gcf,'Name','ConnectTheDots: Raw MaxZ of color 2');
        rgb_c2 = ind2rgb(c2_cell{1,iter}, MagentaColorMap); % convert to rgb:
        image(rgb_c2); hold on;
        set(gca,'XTickLabel',[]); set(gca,'YTickLabel',[]);
        title(['MaxZ of color 2 at time ', num2str(MetaData.MovieData.TimeStep * (CurrFrame-1)), ' min'],'FontWeight','Normal');
        hold off;
    end
    
    toc;
end

end
