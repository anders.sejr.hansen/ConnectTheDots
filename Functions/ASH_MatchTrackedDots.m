function [TrackedDotPairs, TrackData, FrameData] = ASH_MatchTrackedDots(FrameData, MetaData, TrackData, Step6Plots)
%ASH_MATCHTRACKEDDOTS Take tracked dots and match pairs from 2 diff colors
%   This function goes through the following steps:
%       Step A: Calculate 3D distances between pairs
%       Step B: Iteratively match dots
%       Step C: Plot all the matched trajectories

%   Written by Anders Sejr Hansen (AndersSejrHansen@post.harvard.edu)
%   Dependent functions: 
%               ASH_CalcTimeAveraged3D_Dist.m
%               ASH_IterativelyMatchPairsOfDots.m
%               subaxis.m



%%%%%%%%%% STEP A - CALCULATE 3D DISTANCES BETWEEN PAIRS %%%%%%%%%%%%%%%%%%
disp('Step 6.1: calculate time-averaged 3D distances between trajectories'); tic;
% Take all long trajectories and then calculate 3D distances between pairs
% averages over all the timepoints
numFrames = length(TrackData.Points3D_c1);
[TrackData.Matrix3D_Dist] = ASH_CalcTimeAveraged3D_Dist(TrackData.LongTracks_c1, TrackData.LongTracks_c2, numFrames);

%%%%%%%%%% STEP B - MATCH PAIRS OF DOTS %%%%%%%%%%%%%%%%%%
disp('Step 6.2: match pairs of dots');
[TrackedDotPairs] = ASH_IterativelyMatchPairsOfDots(TrackData.LongTracks_c1, TrackData.LongTracks_c2, TrackData.Matrix3D_Dist, MetaData.TrackingOptions.MatchMaxDist);


%%%%%%%%%% STEP C - MATCH PAIRS OF DOTS %%%%%%%%%%%%%%%%%%
disp('Step 6.3: plot and overlay tracked paired dots on images');

PixelSize = MetaData.PixelSize;
TimeStep = MetaData.MovieData.TimeStep;
PlotPath = MetaData.SaveOptions.PlotPath;
PlotDir = 'Plots';
output_path = [PlotPath, filesep, PlotDir, filesep];
PlotPreName = MetaData.SaveOptions.PlotPreName;

% make a folder for saving the tracking plots:
if exist(output_path, 'dir');
    % OK, output_path exists and is a directory (== 7). 
    disp('The given output folder exists. Images/plots will be saved to:');
    disp(output_path);
else
    mkdir(output_path);
    disp('The given output folder did not exist, but was just created. Images/plots will be saved to:');
    disp(output_path);
end
% only do the very slow plotting if == 1
if Step6Plots == 1
    % Create colormaps
    GreenColorMap = zeros(64,3);
    MagentaColorMap = zeros(64,3);
    for i=1:size(GreenColorMap,1)
        GreenColorMap(i,:) = ((i-1)/(size(GreenColorMap,1)-1)) .* [0; 1; 0;];
        MagentaColorMap(i,:) = ((i-1)/(size(MagentaColorMap,1)-1)) .* [1; 0; 1;];
    end

    % assign a random color to each trajectory
    figure; colour = jet; close;
    chosen_colours = zeros(length(TrackedDotPairs), 3);
    for iter = 1:length(TrackedDotPairs)
        chosen_colours(iter,:) = colour( ceil(rand*size(colour,1)), :);
    end

    % find the max for the 3D dist plot:
    max_Dist3D_for_plot = 1000;
    for traj_iter = 1:length(TrackedDotPairs)
        if max(TrackedDotPairs(traj_iter,1).Dist3D) > max_Dist3D_for_plot
            max_Dist3D_for_plot = 1.05*max(TrackedDotPairs(traj_iter,1).Dist3D);
        end
    end

    for FrameIter = 1:length(FrameData)

        curr_fig = figure('position',[200 200 1100 1000]); %[x y width height]
        set(gcf,'Name',['ConnectTheDots: paired dots trajectories in frame', num2str(FrameIter)]);

        % re-scale the images:
        curr_c1 = max(FrameData(FrameIter,1).c1_img, [], 3);
        curr_c2 = max(FrameData(FrameIter,1).c2_img, [], 3);
        % subtract the background:
        curr_c1 = curr_c1 - 0.95*min(curr_c1(:));
        curr_c2 = curr_c2 - 0.95*min(curr_c2(:));
        % normalize by 2x 99.99 percentile:
        Max_c1 = prctile(curr_c1(:), 99.99);
        norm_c1 = curr_c1/(2*Max_c1);
        norm_c1(norm_c1>1) = 1;    
        c1_uint8 = uint8(floor(norm_c1 * 255)); % convert to 8bit
        rgb_c1 = ind2rgb(c1_uint8, GreenColorMap); % convert to rgb:
        % normalize by 2x 99.9999 percentile:
        Max_c2 = prctile(curr_c2(:), 99.99);
        norm_c2 = curr_c2/(1.5*Max_c2);
        norm_c2(norm_c2>1) = 1;    
        c2_uint8 = uint8(floor(norm_c2 * 255)); % convert to 8bit
        rgb_c2 = ind2rgb(c2_uint8, MagentaColorMap); % convert to rgb:

        % save these RGB maxZ images to FrameData
        FrameData(FrameIter,1).c1_maxZ_rgb = rgb_c1;
        FrameData(FrameIter,1).c2_maxZ_rgb = rgb_c2;

         %%%%%% SUB-PLOT 1 - COLOR 1 %%%%%%%%
        h1 = subaxis(2,2,1);
        image(rgb_c1); hold on;
        for traj_iter = 1:length(TrackedDotPairs)
            % plot the current trajectory up to the current frame
            curr_Frames = TrackedDotPairs(traj_iter,1).Frames;
            max_idx = find(FrameIter == curr_Frames);
            if ~isempty(max_idx)
                plot_XY = (TrackedDotPairs(traj_iter,1).c1_XYZ(1:max_idx,1:2))/PixelSize;
                plot(plot_XY(:,2)+1, plot_XY(:,1)+1, 'o', 'MarkerSize', 6, 'Color', chosen_colours(traj_iter,:));
                text(plot_XY(end,2)+5, plot_XY(end,1)+5, num2str(traj_iter), 'Color', 'w');
                if FrameIter > 1 && size(plot_XY,1) > 1
                   plot(plot_XY(:,2), plot_XY(:,1), '-', 'LineWidth', 2, 'Color', chosen_colours(traj_iter,:)); 
                end
            end
        end
        set(gca,'XTickLabel',[]); set(gca,'YTickLabel',[]);
        title(['c1 tracked paired dots at time ', num2str(TimeStep * (FrameIter-1)), ' min'],'FontWeight','Normal');
        hold off;

        %%%%%% SUB-PLOT 2 - COLOR 2 %%%%%%%%
        h2 = subaxis(2,2,2);
        image(rgb_c2); hold on;
        for traj_iter = 1:length(TrackedDotPairs)
            % plot the current trajectory up to the current frame
            curr_Frames = TrackedDotPairs(traj_iter,1).Frames;
            max_idx = find(FrameIter == curr_Frames);
            if ~isempty(max_idx)
                plot_XY = (TrackedDotPairs(traj_iter,1).c2_XYZ(1:max_idx,1:2))/PixelSize;
                plot(plot_XY(:,2)+1, plot_XY(:,1)+1, 'o', 'MarkerSize', 6, 'Color', chosen_colours(traj_iter,:));
                text(plot_XY(end,2)+5, plot_XY(end,1)+5, num2str(traj_iter), 'Color', 'w');
                if FrameIter > 1 && size(plot_XY,1) > 1
                   plot(plot_XY(:,2), plot_XY(:,1), '-', 'LineWidth', 2, 'Color', chosen_colours(traj_iter,:)); 
                end
            end
        end
        set(gca,'XTickLabel',[]); set(gca,'YTickLabel',[]);
        title(['c2 tracked paired dots at time ', num2str(TimeStep * (FrameIter-1)), ' min'],'FontWeight','Normal');
        hold off;

        %%%%%% SUB-PLOT 3 - OVERLAY %%%%%%%%
        h3 = subaxis(2,2,3);
        image(rgb_c1+rgb_c2);
        title(['c2 tracked paired dots at time ', num2str(TimeStep * (FrameIter-1)), ' min'],'FontWeight','Normal');
        set(gca,'XTickLabel',[]); set(gca,'YTickLabel',[]);

        linkaxes([h1,h2,h3], 'xy');

        %%%%%% SUB-PLOT 4 - 3D Distances %%%%%%%%
        subaxis(2,2,4);
        hold on;
        for traj_iter = 1:length(TrackedDotPairs)
            % plot the current trajectory up to the current frame
            curr_Frames = TrackedDotPairs(traj_iter,1).Frames;
            max_idx = find(FrameIter == curr_Frames);
            if ~isempty(max_idx)
                curr_time = TrackedDotPairs(traj_iter,1).TimeStamp(1:max_idx);
                curr_Dist3D = TrackedDotPairs(traj_iter,1).Dist3D(1:max_idx);
                % plot dots:
                plot(curr_time, curr_Dist3D, 'o', 'MarkerSize', 4, 'Color',  chosen_colours(traj_iter,:));
                if FrameIter > 1 && length(curr_Dist3D) > 1
                    plot(curr_time, curr_Dist3D, '-', 'LineWidth', 1.5, 'Color',  chosen_colours(traj_iter,:));
                end
                text(0.25, curr_Dist3D(1,1), num2str(traj_iter), 'Color', chosen_colours(traj_iter,:));
            end
        end
        axis([-0.1 1.05*(length(FrameData)-1)*TimeStep 0 max_Dist3D_for_plot]); 
        title(['3D distance between pairs of dots at time ', num2str(TimeStep * (FrameIter-1)), ' min'],'FontWeight','Normal');
        xlabel('time (min)', 'FontSize',9, 'FontName', 'Helvetica', 'Color', 'k');
        ylabel('3D distance between pairs (nm)', 'FontSize',9, 'FontName', 'Helvetica', 'Color', 'k');
        hold off;

        % save the plot
        set(curr_fig,'Units','Inches');
        pos = get(curr_fig,'Position');
        set(curr_fig,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)]);
        print(curr_fig,[output_path, PlotPreName, '_MatchedDotsFrame', num2str(FrameIter), '.png'],'-dpng','-r300');
    end
end
toc;
end

