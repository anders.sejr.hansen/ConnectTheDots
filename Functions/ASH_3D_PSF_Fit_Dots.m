function [FrameData, MetaData] = ASH_3D_PSF_Fit_Dots(FrameData, MetaData)
%UNTITLED Perform 3D PSF-fitting to rough dot XYZ coordinates
%   The goal of this function is to take as input the rough XYZ coordinates
%   of the dots and then return the precise sub-pixel resolution XYZ
%   coordinates of the dots, which will be obtained through PSF-fitting.
%   This function goes through the following steps:
%       Step A: Extract cropped 3D image around each Dot
%       Step B: Perform fine-scale 3D PSF-fitting

%   Written by Anders Sejr Hansen
%   Dependent functions: 
%               ASH_Crop3D_ImgAroundDot.m
%               ASH_CoM_PSF_fit_each_dot.m
%               ait_centroid3d_v3.m
%               fun_Gaussian_3D_v2.m

tic;

%%%%%%%%%% STEP A - EXTRACT CROPPED 3D IMAGE AROUND EACH DOT %%%%%%%%%%%%%%
disp('Step 4.1: extract 3D image stacks around each dot');
% get a small cropped image around each detected dot. The image will be a
% 3D stack to which we will later do fine-scale PSF-fitting. Also, remember
% to add some padding to the image to go beyond the strict dimensions of
% the theoretical PSF. 

% XY-padding: you need to avoid a situation where the xy-padding for c1 and
% c2 are different. This is unlikely to happen, but could occur due to
% differences in wavelengths. Therefore, for the purposes of cropping
% images, you should use the XY padding of the widest PSF, to ensure the c1
% and c2 cropped images have the same sizes:
xy_padding= round(2*max([MetaData.c1_PSF_theo_xy_pix MetaData.c2_PSF_theo_xy_pix]))+MetaData.PSF_xy_padding;
z_padding= round(2*max([MetaData.c1_PSF_theo_z_pix MetaData.c2_PSF_theo_z_pix]))+MetaData.PSF_z_padding;

for iter = 1:length(FrameData)
    % get images around the good Dots:
    [FrameData(iter,1).c1_img_3D_crop] = ASH_Crop3D_ImgAroundDot(FrameData(iter,1).c1_img, FrameData(iter,1).QC_Dots_XYZ_c1, xy_padding, z_padding);
    [FrameData(iter,1).c2_img_3D_crop] = ASH_Crop3D_ImgAroundDot(FrameData(iter,1).c2_img, FrameData(iter,1).QC_Dots_XYZ_c2, xy_padding, z_padding);
end


%%%%%%%%%% STEP B - PERFORM PSF-FITTING FOR EACH DOT %%%%%%%%%%%%%%%%%%%%%%
disp('Step 4.2: fine-scale PSF fit each dot');

% QC_Dots_PSF_fit is a N x 7 matrix, where each row is a dot. But you will
% need to save a legend with a meaning of each of the 7 or 8 columns:
if MetaData.XY_symmetric_PSF == 1
    % The PSF is symmetric and you will enforce that sigma X and sigma Y
    % are identical for the PSF fitting:
    PSF_Fit_Flag = 2;
    MetaData.PSF_fit_columns = {'PSF_sigma_XY', 'PSF_sigma_Z', 'X', 'Y', 'Z', 'Amplitude', 'Background'};
elseif MetaData.XY_symmetric_PSF == 0
    % The PSF is not neccesarily symmetrix and you are fitting sigma_X and
    % sigma_Y independently:
    PSF_Fit_Flag = 1;
    MetaData.PSF_fit_columns = {'PSF_sigma_X', 'PSF_sigma_Y', 'PSF_sigma_Z', 'X', 'Y', 'Z', 'Amplitude', 'Background'};
end
MetaData.FinalDots_columns = {'PSF_sigma_X', 'PSF_sigma_Y', 'PSF_sigma_Z', 'X', 'Y', 'Z', 'Amplitude', 'Background'};

% generate mesh-grid with the x-data for the PSF fitting. Since this is the
% same for all of the cropped images, there is no need to re-calculate it
% for each PSF-fit. First, grap a dummy cropped 3D img:
curr_img = FrameData(iter,1).c1_img_3D_crop{1,1}; % grap a dummy img
% Dimension of cropped image
[dim.Y, dim.X, dim.Z] = size(curr_img);
N_pix                 = dim.X*dim.Y*dim.Z;
% generate a grid for the x-data
[Xs,Ys,Zs] = meshgrid(dim.Y:2*dim.Y-1,dim.X:2*dim.X-1,dim.Z:2*dim.Z-1);  % Pixel-grid has on offset from 0 to allow for negative values in center position
xdata      = zeros(3,N_pix);
xdata(1,:) = double(reshape(Xs,1,N_pix))*MetaData.PixelSize;
xdata(2,:) = double(reshape(Ys,1,N_pix))*MetaData.PixelSize;
xdata(3,:) = double(reshape(Zs,1,N_pix))*MetaData.zStep;
% xdata is a 3 x N matrix, where N is the number of pixels. It provides
% an alternative format to working directly with 3D images
    
    
    
% Loop over each frame:
for iter = 1:length(FrameData)
    % color 1
    [FrameData(iter,1).FinalDots_c1, FrameData(iter,1).QC_Dots_PSF_fit_c1, FrameData(iter,1).QC_Dots_XYZ_CoM_c1, FrameData(iter,1).c1_img_fit_3D_crop, FrameData(iter,1).c1_img_3D_crop_PSF_XYZ] ...
        = ASH_CoM_PSF_fit_each_dot(xdata, dim, FrameData(iter,1).c1_img_3D_crop, FrameData(iter,1).QC_Dots_XYZ_c1, ...
        MetaData.PixelSize, MetaData.zStep, MetaData.c1_PSF_theo_xy_nm, MetaData.c1_PSF_theo_z_nm, PSF_Fit_Flag);
    % color 2
    [FrameData(iter,1).FinalDots_c2, FrameData(iter,1).QC_Dots_PSF_fit_c2, FrameData(iter,1).QC_Dots_XYZ_CoM_c2, FrameData(iter,1).c2_img_fit_3D_crop, FrameData(iter,1).c2_img_3D_crop_PSF_XYZ] ...
        = ASH_CoM_PSF_fit_each_dot(xdata, dim, FrameData(iter,1).c2_img_3D_crop, FrameData(iter,1).QC_Dots_XYZ_c2, ...
        MetaData.PixelSize, MetaData.zStep, MetaData.c2_PSF_theo_xy_nm, MetaData.c2_PSF_theo_z_nm, PSF_Fit_Flag);
end


toc;
end

