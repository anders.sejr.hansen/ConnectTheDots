function [img_3D_crop] = ASH_Crop3D_ImgAroundDot(img, Dots_XYZ, xy_padding, z_padding)
%ASH_CROP3D_IMGAROUNDDOT Crop a 3D image around the dot for fitting
%   Go through all the good dots and then crop out a 3D image around each
%   dot
%   Written by Anders Sejr Hansen

% store the cropped images (3D matrices) in a cell array:
img_3D_crop = cell(size(Dots_XYZ,1),1);
[dim.Y, dim.X, dim.Z] = size(img); % remember that X,Y and row,col are confusing and switching between matrices and images

% loop over all the good dots:
for dot_iter = 1:size(Dots_XYZ,1)
    % get the limits:
    y_min = Dots_XYZ(dot_iter,1)-xy_padding;
    y_max = Dots_XYZ(dot_iter,1)+xy_padding;

    x_min = Dots_XYZ(dot_iter,2)-xy_padding;
    x_max = Dots_XYZ(dot_iter,2)+xy_padding;

    z_min = Dots_XYZ(dot_iter,3)-z_padding;
    z_max = Dots_XYZ(dot_iter,3)+z_padding;
    
    % make sure that you don't try to get pixels that are not within the
    % ImageStack:
    if y_min < 1;     y_min = 1;     end
    if y_max > dim.Y; y_max = dim.Y; end    
    if x_min < 1;     x_min = 1;     end
    if x_max > dim.X; x_max = dim.X; end    
    if z_min < 1;     z_min = 1;     end
    if z_max > dim.Z; z_max = dim.Z; end
    
    % extract the cropped 3D image:
    img_3D_crop{dot_iter,1} = double(img(y_min:y_max,x_min:x_max,z_min:z_max));
end

% finally return the cell array with the cropped images

end

