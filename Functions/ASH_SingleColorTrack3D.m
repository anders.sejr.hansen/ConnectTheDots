function [TrackData] = ASH_SingleColorTrack3D(FrameData, MetaData, CurrStep)
%ASH_SINGLECOLORTRACK3D Perform 3D tracking in a single color-channel
%   This function goes through the following steps:
%       Step A: Convert dots to cell array data-structure
%       Step B: Track dots in one color in 3D across frames
%       Step C: Plot the success of the output

%   Written by Anders Sejr Hansen
%   Heaviliy relies on SimpleTracker written by Jean-Yves Tinevez, https://www.mathworks.com/matlabcentral/fileexchange/34040-simple-tracker
%   Dependent functions: 
%               ASH_Track3D.m
%               ASH_Convert2ToCellArray.m
%               hungarianlinker.m
%               munkres.m
%               nearestneighborlinker.m
%               simpletracker.m
%               plotcube.m

%%%%%%%%%% STEP A - CONVERT TO A CELL ARRAY FORMAT %%%%%%%%%%%%%%%%%%%%%%%%
disp('Step 5.1: convert to cell array format for 3D tracking'); tic;
% convert the dot localizations to a cell array format
[TrackData.Points3D_c1, TrackData.PSFfit_c1, TrackData.img_3D_crop_c1, TrackData.img_fit_3D_crop_c1, TrackData.img_3D_crop_PSF_XYZ_c1, ...
 TrackData.Points3D_c2, TrackData.PSFfit_c2, TrackData.img_3D_crop_c2, TrackData.img_fit_3D_crop_c2, TrackData.img_3D_crop_PSF_XYZ_c2] ...
                    = ASH_Convert2ToCellArray(FrameData);

%%%%%%%%%% STEP B - PERFORM SINGLE COLOR 3D TRACKING %%%%%%%%%%%%%%%%%%%%%%
disp('Step 5.2: track dots in 3D across time');
% track the dots in 3D
[TrackData.AllTracks_c1, TrackData.LongTracks_c1] = ASH_3D_track(TrackData.Points3D_c1, TrackData.PSFfit_c1, TrackData.img_3D_crop_c1, TrackData.img_fit_3D_crop_c1, TrackData.img_3D_crop_PSF_XYZ_c1, ...
                            MetaData.TrackingOptions.MaxGaps, MetaData.TrackingOptions.MinLocFrac, ...
                            MetaData.TrackingOptions.c1_method, MetaData.TrackingOptions.c1_max_link_dist, MetaData.MovieData.TimeStep);
[TrackData.AllTracks_c2, TrackData.LongTracks_c2] = ASH_3D_track(TrackData.Points3D_c2, TrackData.PSFfit_c2, TrackData.img_3D_crop_c2, TrackData.img_fit_3D_crop_c2, TrackData.img_3D_crop_PSF_XYZ_c2, ...
                            MetaData.TrackingOptions.MaxGaps, MetaData.TrackingOptions.MinLocFrac, ...
                            MetaData.TrackingOptions.c2_method, MetaData.TrackingOptions.c2_max_link_dist, MetaData.MovieData.TimeStep);

%%%%%%%%%% STEP C - PLOT THE SUCCESS OF THE TRACKING %%%%%%%%%%%%%%%%%%%%%%
if CurrStep == 2
    figure; colour = jet; close; % matlab is annoying with colormaps. So do this to avoid empty figures. 
    disp('Step 5.3: plot the success of the single-color tracking in 3D');
    figure('position',[200 200 1000 800]); %[x y width height]
    set(gcf,'Name','ConnectTheDots: optimize single-color tracking parameters');
    
    % determine the max of the axis:
    xy_max = 0; z_max = 0;
    for iter = 1:length(TrackData.AllTracks_c1)
        if max(TrackData.AllTracks_c1(iter,1).XYZ(:,1:2)) > xy_max
            xy_max = 1.05*max(max(TrackData.AllTracks_c1(iter,1).XYZ(:,1:2)));
        end
        if max(TrackData.AllTracks_c1(iter,1).XYZ(:,3)) > z_max
            z_max = 1.05*max(TrackData.AllTracks_c1(iter,1).XYZ(:,3));
        end
    end
    for iter = 1:length(TrackData.AllTracks_c2)
        if max(TrackData.AllTracks_c2(iter,1).XYZ(:,1:2)) > xy_max
            xy_max = 1.05*max(max(TrackData.AllTracks_c2(iter,1).XYZ(:,1:2)));
        end
        if max(TrackData.AllTracks_c2(iter,1).XYZ(:,3)) > z_max
            z_max = 1.05*max(TrackData.AllTracks_c2(iter,1).XYZ(:,3));
        end
    end
    
    
    % PLOT ALL THE DOTS FOR COLOR 1;
    subplot(2,2,1);
    % plot cube:
        P = [xy_max/2,xy_max/2,z_max/2];   % you center point 
        L = [xy_max,xy_max,z_max];  % your cube dimensions 
        O = P-L/2 ;       % Get the origin of cube so that P is at center 
        plotcube(L,O,.2,[1 1 1]);   % use function plotcube 
        hold on;
    % plot your points
    for FrameIter = 1:length(FrameData)
        idx = round((FrameIter-1)/length(FrameData)*size(colour,1));
        colour_element = colour(max([idx, 1]),:);
        plot3(TrackData.Points3D_c1{FrameIter}(:,1), TrackData.Points3D_c1{FrameIter}(:,2), TrackData.Points3D_c1{FrameIter}(:,3), 'o', 'MarkerSize', 3, 'Color', colour_element, 'MarkerFaceColor', colour_element);
    end
    axis([0 xy_max 0 xy_max 0 z_max]); grid on; 
    xlabel('x (nm)', 'FontSize',9, 'FontName', 'Helvetica', 'Color', 'k');
    ylabel('y (nm)', 'FontSize',9, 'FontName', 'Helvetica', 'Color', 'k');
    zlabel('z (nm)', 'FontSize',9, 'FontName', 'Helvetica', 'Color', 'k');
    title({'c1: all QC dots; Time runs from blue to red'; ['MaxGaps: ', num2str(MetaData.TrackingOptions.MaxGaps), '; Min Loc Frac: ', num2str(MetaData.TrackingOptions.MinLocFrac)]}, 'FontName', 'Helvetica', 'Color', 'k','FontWeight','Normal');
    hold off;
    
    % PLOT ALL THE LongTracks DOTS FOR COLOR 1;
    subplot(2,2,2);
    % plot cube:
        P = [xy_max/2,xy_max/2,z_max/2];   % you center point 
        L = [xy_max,xy_max,z_max];  % your cube dimensions 
        O = P-L/2 ;       % Get the origin of cube so that P is at center 
        plotcube(L,O,.2,[1 1 1]);   % use function plotcube 
        hold on;
    % plot your long tracks:
    for traj_iter = 1:length(TrackData.LongTracks_c1)
        curr_XYZ = TrackData.LongTracks_c1(traj_iter).XYZ;
        curr_Frames = TrackData.LongTracks_c1(traj_iter).Frames;
        % first plot the points
        for FrameIter = 1:length(curr_Frames)
            idx = round((curr_Frames(FrameIter)-1)/length(FrameData)*size(colour,1));
            colour_element = colour(max([idx, 1]),:);
            plot3(curr_XYZ(FrameIter,1), curr_XYZ(FrameIter,2), curr_XYZ(FrameIter,3), 'o', 'MarkerSize', 3, 'Color', colour_element, 'MarkerFaceColor', colour_element);
        end
        % second overlay the connections:
        plot3(curr_XYZ(:,1), curr_XYZ(:,2), curr_XYZ(:,3), 'k-', 'LineWidth', 1);
    end
    axis([0 xy_max 0 xy_max 0 z_max]); grid on; 
    xlabel('x (nm)', 'FontSize',9, 'FontName', 'Helvetica', 'Color', 'k');
    ylabel('y (nm)', 'FontSize',9, 'FontName', 'Helvetica', 'Color', 'k');
    zlabel('z (nm)', 'FontSize',9, 'FontName', 'Helvetica', 'Color', 'k');
    title({'c1: all long-tracked dots; Time runs from blue to red'; ['Tracking method: ', MetaData.TrackingOptions.c1_method, '; Max distance: ', num2str(MetaData.TrackingOptions.c1_max_link_dist), ' nm']}, 'FontName', 'Helvetica', 'Color', 'k','FontWeight','Normal');
    hold off;
    
    
    % PLOT ALL THE DOTS FOR COLOR 1;
    subplot(2,2,3);
    % plot cube:
        P = [xy_max/2,xy_max/2,z_max/2];   % you center point 
        L = [xy_max,xy_max,z_max];  % your cube dimensions 
        O = P-L/2 ;       % Get the origin of cube so that P is at center 
        plotcube(L,O,.2,[1 1 1]);   % use function plotcube 
        hold on;
    % plot your points
    for FrameIter = 1:length(FrameData)
        idx = round((FrameIter-1)/length(FrameData)*size(colour,1));
        colour_element = colour(max([idx, 1]),:);
        plot3(TrackData.Points3D_c2{FrameIter}(:,1), TrackData.Points3D_c2{FrameIter}(:,2), TrackData.Points3D_c2{FrameIter}(:,3), 'o', 'MarkerSize', 3, 'Color', colour_element, 'MarkerFaceColor', colour_element);
    end
    axis([0 xy_max 0 xy_max 0 z_max]); grid on; 
    xlabel('x (nm)', 'FontSize',9, 'FontName', 'Helvetica', 'Color', 'k');
    ylabel('y (nm)', 'FontSize',9, 'FontName', 'Helvetica', 'Color', 'k');
    zlabel('z (nm)', 'FontSize',9, 'FontName', 'Helvetica', 'Color', 'k');
    title({'c2: all QC dots; Time runs from blue to red'; ['MaxGaps: ', num2str(MetaData.TrackingOptions.MaxGaps), '; Min Loc Frac: ', num2str(MetaData.TrackingOptions.MinLocFrac)]}, 'FontName', 'Helvetica', 'Color', 'k','FontWeight','Normal');
    hold off;
    
    % PLOT ALL THE LongTracks DOTS FOR COLOR 1;
    subplot(2,2,4);
    % plot cube:
        P = [xy_max/2,xy_max/2,z_max/2];   % you center point 
        L = [xy_max,xy_max,z_max];  % your cube dimensions 
        O = P-L/2 ;       % Get the origin of cube so that P is at center 
        plotcube(L,O,.2,[1 1 1]);   % use function plotcube 
        hold on;
    % plot your long tracks:
    for traj_iter = 1:length(TrackData.LongTracks_c2)
        curr_XYZ = TrackData.LongTracks_c2(traj_iter).XYZ;
        curr_Frames = TrackData.LongTracks_c2(traj_iter).Frames;
        % first plot the points
        for FrameIter = 1:length(curr_Frames)
            idx = round((curr_Frames(FrameIter)-1)/length(FrameData)*size(colour,1));
            colour_element = colour(max([idx, 1]),:);
            plot3(curr_XYZ(FrameIter,1), curr_XYZ(FrameIter,2), curr_XYZ(FrameIter,3), 'o', 'MarkerSize', 3, 'Color', colour_element, 'MarkerFaceColor', colour_element);
        end
        % second overlay the connections:
        plot3(curr_XYZ(:,1), curr_XYZ(:,2), curr_XYZ(:,3), 'k-', 'LineWidth', 1);
    end
    axis([0 xy_max 0 xy_max 0 z_max]); grid on; 
    xlabel('x (nm)', 'FontSize',9, 'FontName', 'Helvetica', 'Color', 'k');
    ylabel('y (nm)', 'FontSize',9, 'FontName', 'Helvetica', 'Color', 'k');
    zlabel('z (nm)', 'FontSize',9, 'FontName', 'Helvetica', 'Color', 'k');
    title({'c2: all long-tracked dots; Time runs from blue to red'; ['Tracking method: ', MetaData.TrackingOptions.c2_method, '; Max distance: ', num2str(MetaData.TrackingOptions.c2_max_link_dist), ' nm']}, 'FontName', 'Helvetica', 'Color', 'k','FontWeight','Normal');
    hold off;
end

toc;
end

