function [ QC_Dots_XYZ ] = ASH_RingQC_Dots( img, all_dots_XYZ, RingRadius, RelativeIntensityThres )
%ASH_RINGQC_DOTS Remove dots that do not resemble a PSF 
%   Get the intensity of a ring around the dot and compare this to the
%   intensity of the central dot

%   Written by Anders Sejr Hansen

% calculate the general background (use 20 prctile):
img_background = prctile(img(:), 20);
max_x = size(img,1); max_y = size(img,2);

% make a yes/no binary vector to see if the dots made the QC test:
GoodDots = zeros(size(all_dots_XYZ,1),1);

for dot_iter = 1:size(all_dots_XYZ,1);
    % test c1 dot: 149    96     6
    curr_dot = all_dots_XYZ(dot_iter,:);

    curr_zSlice = img(:,:,curr_dot(3));

    Dot_intensity_matrix = curr_zSlice( (curr_dot(1)-1):(curr_dot(1)+1), (curr_dot(2)-1):(curr_dot(2)+1));
    Dot_intensity = mean(Dot_intensity_matrix(:)) - img_background;

    %Generate pixels on the circle perimeter:
    sinB = sin(0); cosB = cos(0);
    i = pi/20:pi/20:2*pi; % divide by 20 to get 40 pixel coordinates
    sinA = sin(i); cosA = cos(i);
    x_perimeter = round(curr_dot(1) + (RingRadius * cosA*cosB - RingRadius * sinA*sinB));
    y_perimeter = round(curr_dot(2) + (RingRadius * cosA*sinB + RingRadius * sinA*cosB));
    
    % if the dot is close to the edge and/or if the ring is too wide, you
    % can get into trouble. So need to ensure that no dots exist outside of
    % the edges:
    good_x   = (x_perimeter > 0) & (x_perimeter <= max_x);
    good_y   = (y_perimeter > 0) & (y_perimeter <= max_y);
    good_perimeter_vals = good_x & good_y;
    % now update the X/Y perimeters to limit them to pixels inside the
    % image:
    x_perimeter = x_perimeter(good_perimeter_vals);
    y_perimeter = y_perimeter(good_perimeter_vals);

    % old version, used a for loop, which is slow. I left it in here just to
    % show how the values are extracted since sub2ind is not intuitive, but
    % since it is ~30 times faster, let's use sub2ind instead.
        %ring_vals = zeros(length(x_perimeter),1);
        %for iter = 1:length(x_perimeter)
        %    ring_vals(iter,1) = img(x_perimeter(iter), y_perimeter(iter), curr_dot(3));
        %end
    % get values of the pixel ring:
    ring_vals = curr_zSlice(sub2ind(size(curr_zSlice), x_perimeter, y_perimeter)); % get pixel intensities of the ring
    ring_intensity = mean(ring_vals) - img_background;

    % now do a simple yes/no decision to see if this dot will make it:
    if Dot_intensity / ring_intensity > RelativeIntensityThres % is the dot much brighter than the ring?
        % sometimes, since you background subtract, the background
        % subtracted ring intensity can be near zero. So also just make
        % sure that the dot intensity is X-fold higher than background:
        if Dot_intensity+img_background > 1.35*img_background
            GoodDots(dot_iter,1) = 1;
        end
    end
end

% finally return the dots that passed the QC threshold:
QC_Dots_XYZ = all_dots_XYZ(logical(GoodDots),:);
end