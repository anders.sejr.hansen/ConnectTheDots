function [Matrix3D_Dist] = ASH_CalcTimeAveraged3D_Dist(Tracks1, Tracks2, numFrames)
%ASH_CALCTIMEAVERAGED3D_DIST Calculate time-averaged 3D distances

% The trajectories contain gaps, so be careful when calculating the
% distances. Use NaN for all the missing XYZ's. 
Points1 = NaN(length(Tracks1), 3, numFrames);
Points2 = NaN(length(Tracks2), 3, numFrames);

% now generate matrices with the XYZ coordinates
for iter = 1:length(Tracks1)
    % extract the key info
   curr_XYZ =  Tracks1(iter,1).XYZ;
   curr_Frames =  Tracks1(iter,1).Frames;
   % populate the relevant frames with points:
   for FrameIter = 1:length(curr_Frames)
       Points1(iter,:,curr_Frames(FrameIter)) = curr_XYZ(FrameIter,:);
   end
end
% now generate matrices with the XYZ coordinates
for iter = 1:length(Tracks2)
    % extract the key info
   curr_XYZ =  Tracks2(iter,1).XYZ;
   curr_Frames =  Tracks2(iter,1).Frames;
   % populate the relevant frames with points:
   for FrameIter = 1:length(curr_Frames)
       Points2(iter,:,curr_Frames(FrameIter)) = curr_XYZ(FrameIter,:);
   end
end

% pre-declare 3D matrix:
Matrix3D_Dist = NaN(length(Tracks1), length(Tracks2), numFrames);
% now loop over each frame and generate the Euclidean distances:
for iter = 1:numFrames
    Matrix3D_Dist(:,:,iter) = pdist2(Points1(:,:,iter),Points2(:,:,iter), 'euclidean');
end


end

