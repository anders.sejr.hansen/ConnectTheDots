function [ TrackedDotPairs ] = ASH_Calc_Signal2Background( TrackedDotPairs )
%ASH_CALC_SIGNAL2BACKGROUND Calculate Signal-to-Background
%   Calculate the signal-to-background for each localization:
%   2 major approaches: 
%       1. compare signal from PSF-fit to the fitted background. 
%           Advantage: easy and intuitive
%           Disadvantage: not always accurate, especially if PSF-fit not
%           perfect
%       2. compare measured signal to fitted background
%           Advantage: the real signal
%           Disadvantage: if fitted background is inaccurate, there could
%           be trouble
%   Go with the 2nd option (measured signal divided by fitted background)
%   Also, based on a few example trajectories, the 2 options actually
%   appear to give near-identical results. 
%   Store signal-to-background as a Nx2 matrix for c1 and c2:

% get dimensions of the cropped image to speed up calculations and multiple
% them:
curr_img = TrackedDotPairs(1).c1.img_3D_crop{1}; % dummy img
N_pix = size(curr_img,1) * size(curr_img,2) * size(curr_img,3);

for PairIter = 1:size(TrackedDotPairs,1)
    % pre-declare variable:
    c1c2_Signal2Background = zeros(length(TrackedDotPairs(PairIter,1).Frames),2);
    % there could be frames where only one dot was tracked, so handle this
    % appropriately:
    [~,c1_idx,~] = intersect(TrackedDotPairs(PairIter,1).c1.Frames, TrackedDotPairs(PairIter,1).Frames);
    [~,c2_idx,~] = intersect(TrackedDotPairs(PairIter,1).c2.Frames, TrackedDotPairs(PairIter,1).Frames);
    
    % calculate the signal-to-background:
    for iter = 1:length(c1_idx)
        total_signal = sum(TrackedDotPairs(PairIter,1).c1.img_3D_crop{c1_idx(iter)}(:)) ...
                                            - (TrackedDotPairs(PairIter,1).c1.Background(c1_idx(iter)) * N_pix);
        c1c2_Signal2Background(iter,1) = total_signal / TrackedDotPairs(PairIter,1).c1.Background(c1_idx(iter));
    end
    for iter = 1:length(c2_idx)
        total_signal = sum(TrackedDotPairs(PairIter,1).c2.img_3D_crop{c2_idx(iter)}(:)) ...
                                            - (TrackedDotPairs(PairIter,1).c2.Background(c2_idx(iter)) * N_pix);
        c1c2_Signal2Background(iter,2) = total_signal / TrackedDotPairs(PairIter,1).c2.Background(c2_idx(iter));
    end
    TrackedDotPairs(PairIter,1).c1c2_Signal2Background = c1c2_Signal2Background;
end
% finish
end

