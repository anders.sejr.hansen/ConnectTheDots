function [FrameData] = ASH_FilterImages(FrameData, MetaData, CurrStep)
%ASH_FILTERIMAGES Filter images for contrast enhancement
%   This function takes as input 2 structures/objects with information on
%   how to do the filtering for both colors and then fiters each 3D
%   imagestack for each timepoint and returns the filtered images

%   Written by Anders Sejr Hansen
%   Uses LoG filter written by Nico Battich, Thomas Stoeger, Lucas Pelkmans
%   Dependent function: fspecialCP3D.m
%   Also requires MatLab's image processing toolbox

if strcmp(MetaData.FilterOptions.FilterType, 'LoG');
    disp('Step 2: Performing contrast enhancement using a Laplacian of Gaussian filter'); tic;
    % Use a rotationally symmetric Laplacian of Gaussian filter
    % Define the filtering parameters for each color:
    c1_LoG_filter_radius = MetaData.FilterOptions.c1_radius; % Radius: default is 5 pixels
    c1_LoG_filter_sigma = MetaData.FilterOptions.c1_sigma; % Sigma: default is (Radius-1)/3 
    c1_LoG_filter_numPlanes = MetaData.FilterOptions.c1_zPlanes; % numPlanes: default is 3 (FISH-Quant always uses 3)
    c2_LoG_filter_radius = MetaData.FilterOptions.c2_radius; % Radius: default is 5 pixels
    c2_LoG_filter_sigma = MetaData.FilterOptions.c2_sigma; % Sigma: default is (Radius-1)/3 
    c2_LoG_filter_numPlanes = MetaData.FilterOptions.c2_zPlanes; % numPlanes: default is 3 (FISH-Quant always uses 3)
    
    % check to see if you are just adjusting the thresholds (CurrStep=1) or
    % if you are analyzing all of the data (CurrStep=2); 
    if CurrStep == 1 % only first and last
        FramesToAnalyze = [1, length(FrameData)]; % first and last frames only
    elseif CurrStep == 2 % do all frames
        FramesToAnalyze = 1:length(FrameData);
    end
    
    % Generate the filters only once:
    c1_filter = fspecialCP3D('3D LoG, Raj', c1_LoG_filter_radius, c1_LoG_filter_sigma, c1_LoG_filter_numPlanes);
    c2_filter = fspecialCP3D('3D LoG, Raj', c2_LoG_filter_radius, c2_LoG_filter_sigma, c2_LoG_filter_numPlanes);
    
    % Perform the LoG Filtering
    for iter = 1:length(FramesToAnalyze)
        CurrFrame = FramesToAnalyze(iter);
        % filter the 3D image z-stacks:
        FrameData(CurrFrame,1).c1_img_filt = imfilter(FrameData(CurrFrame,1).c1_img, c1_filter,'symmetric') *(-1);   %- Picture is inversed & has  negative elements; 
        FrameData(CurrFrame,1).c2_img_filt = imfilter(FrameData(CurrFrame,1).c2_img, c2_filter,'symmetric') *(-1);   %- Picture is inversed & has  negative elements; 
    end
    toc;
else
    error(['error: the filter you supplied, ', MetaData.FilterOptions.FilterType, ', is not supported. Use LoG instead']);
end

