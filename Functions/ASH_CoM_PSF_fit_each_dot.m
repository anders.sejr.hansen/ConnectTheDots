function [FinalDots, Dots_PSF_fit, Dots_XYZ_CoM, img_fit_3D_crop, img_3D_crop_PSF_XYZ] = ASH_CoM_PSF_fit_each_dot(xdata, dim, img_3D_crop, Dots_XYZ, PixelSize, zStep, PSF_theo_xy_nm, PSF_theo_z_nm, PSF_Fit_Flag)
%ASH_COM_PSF_FIT_EACH_DOT Determine Center-of-Mass and PSF-fit each dot
%   Write later
%   - img_3D_crop is a cell array where is element is a 3D image stack
%   - Dots_XYZ is the original rough localization coordinates of the dot in
%   pixel units
%   - PixelSize: nm length of XY pixel


%   Written by Anders Sejr Hansen
%   Dependent functions: 
%               ait_centroid3d_v3.m
%               fun_Gaussian_3D_v2.m

% Pre-initialize memory for storing the key variables:
Dots_XYZ_CoM = zeros(size(img_3D_crop,1),3); 
FinalDots = zeros(size(img_3D_crop,1),8);
img_fit_3D_crop = cell(size(img_3D_crop,1),1);
img_3D_crop_PSF_XYZ = zeros(size(img_3D_crop,1),3); 

%%%%%% LIMITS FOR FIT WITH 3D GAUSSIAN %%%%%%%%
sigma_xy_min = 0; sigma_xy_max = round(10*PSF_theo_xy_nm);
sigma_z_min = 0; sigma_z_max = round(10*PSF_theo_z_nm);

%  Least Squares Curve Fitting
options_fit.options = optimset('Jacobian','off','Display','off','MaxIter',200,'UseParallel','always');

% IF THE PSF IS SYMMETRIC IN XY YOU WILL NEED 7 COLUMNS
if PSF_Fit_Flag == 2
    Dots_PSF_fit = zeros(size(img_3D_crop,1),7);
    % Fitting limits sigmaXY - sigmaZ - centerX - centerY - centerZ  - amp - bgd
    bound.lb   = [sigma_xy_min sigma_z_min -inf -inf -inf 0   0  ]; 
    bound.ub   = [sigma_xy_max sigma_z_max inf  inf  inf  inf inf];
elseif PSF_Fit_Flag == 1
    % IF THE PSF IS NOT NECCESARILY SYMMETRIC IN XY YOU WILL NEED 8 COLUMNS
    Dots_PSF_fit = zeros(size(img_3D_crop,1),8);
    % Fitting limits sigmaX - sigmaY - sigmaZ - centerX - centerY - centerZ  - amp - bgd
    bound.lb   = [sigma_xy_min sigma_xy_min sigma_z_min -inf -inf -inf 0   0  ]; 
    bound.ub   = [sigma_xy_max sigma_xy_max sigma_z_max inf  inf  inf  inf inf];
end

% Starting points for fitting routine
%- Sigma: from theoretical model 
%  Need to consider what the optimal starting points will be, since the
%  dots may not always be diffraction limited (so you may need to increase
%  their size):
par_start.sigmax = PSF_theo_xy_nm; 
par_start.sigmay = PSF_theo_xy_nm; 
par_start.sigmaz = PSF_theo_z_nm; 

% loop over each dot with its associated image-stack
for iter = 1:size(img_3D_crop,1)
    % get out the current cropped 3D image stack:
    curr_img = img_3D_crop{iter,1};
    
    
    % Reformating image to fit data-format of axis vectors
    ydata = double(reshape(curr_img,1,size(xdata,2))); % NB, this will be the pixel intensity values
    
    % Amplitude and background: estimated from min and max of the image: 
    img_max   = max(curr_img(:));
    img_min   = (min(curr_img(:))) * double((min(curr_img(:))>0)) + (1*(min(curr_img(:))<=0));
    % store in the struct, par_start, for the fitting
    par_start.amp = img_max-img_min; 
    par_start.bgd = img_min; 
    
    % Position of Gaussian - from center of mass
    center_mass  = ait_centroid3d_v3(double(curr_img),xdata);
    % save the center of mass
    Dots_XYZ_CoM(iter,:) = center_mass;
    % use the Center Of Mass as the initial guess for the fitting routine
    par_start.centerx = center_mass(1); par_start.centery = center_mass(2); par_start.centerz = center_mass(3); 
      
    %- Model parameters
    par_mod{1} = PSF_Fit_Flag;     % If PSF_Fit_Flag =2: then the PSF is XY-symmetric sigma_x = sigma_y
                                   % If PSF_Fit_Flag =1: then the PSF is XY-asymmetric sigma_x may or may not equal sigma_y
    par_mod{2} = xdata;         
    pixel_size = struct('xy', PixelSize, 'z', zStep);
    par_mod{3} = pixel_size;
    
    % IF THE PSF IS SYMMETRIC IN XY YOU WILL NEED 7 COLUMNS
    if PSF_Fit_Flag == 2
        x_init = [ par_start.sigmax,  par_start.sigmaz,  ...
            par_start.centerx, par_start.centery, par_start.centerz,...
            par_start.amp,     par_start.bgd];
    elseif PSF_Fit_Flag == 1
        % IF THE PSF IS NOT NECCESARILY SYMMETRIC IN XY YOU WILL NEED 8 COLUMNS
        x_init = [ par_start.sigmax,  par_start.sigmay,  par_start.sigmaz,  ...
            par_start.centerx, par_start.centery, par_start.centerz,...
            par_start.amp,     par_start.bgd];
    end
    
    %%%%%%%%%%% FINALLY - DO THE LEAST SQUARES PSF-FITTING %%%%%%%%%%%%%%%%
    %[fitted_x,resnorm,residual,exitflag,output] = lsqcurvefit(@fun_Gaussian_3D_v2,double(x_init), ...
    %par_mod,ydata,bound.lb,bound.ub,options_fit.options);
    [fitted_x,~,~,~,~] = lsqcurvefit(@fun_Gaussian_3D_v2,double(x_init), ...
    par_mod,ydata,bound.lb,bound.ub,options_fit.options);
    
    % save the result of the PSF-fit:
    Dots_PSF_fit(iter,:) = fitted_x;
    
    % Generate a same-sized 3D cropped image with the fitted PSF function.
    % You will need this later to plot the actually image of the Dot and
    % the fitted image side-by-side for quality control and to filter out
    % cells where the chromosomes have replicated, such that the 2 sister
    % chromatids overlap with each other:
    img_fit_linear  = fun_Gaussian_3D_v2(double(fitted_x),par_mod);
    img_fit_3D_crop{iter,1} = reshape(img_fit_linear, size(curr_img));
    % also save the XYZ coordinates of the fitted center using the small
    % cropped image (e.g. the coordinates in the small cropped image will
    % be different from the very large image):
    % IF THE PSF IS SYMMETRIC IN XY, THERE ARE 7 COLUMNS
    if PSF_Fit_Flag == 2
        img_3D_crop_PSF_XYZ(iter,:) = fitted_x(3:5);
    elseif PSF_Fit_Flag == 1
        % IF THE PSF IS NOT NECCESARILY SYMMETRIC IN XY AND THERE ARE 8 COLUMNS
        img_3D_crop_PSF_XYZ(iter,:) = fitted_x(4:6);
    end
    %%%%%%%%%%%%%% correct coordinates %%%%%%%%%%%%%%%%%%
    % correct the XYZ coordinates for the size of the cropped image:
    
    % correct Center-of-Mass dots:
    Dots_XYZ_CoM(iter,1) = Dots_XYZ_CoM(iter,1) - dim.X*PixelSize;
    Dots_XYZ_CoM(iter,2) = Dots_XYZ_CoM(iter,2) - dim.Y*PixelSize;
    Dots_XYZ_CoM(iter,3) = Dots_XYZ_CoM(iter,3) - dim.Z*zStep;
    % correct PSF-fit
    % IF THE PSF IS SYMMETRIC IN XY, THERE ARE 7 COLUMNS
    if PSF_Fit_Flag == 2
        Dots_PSF_fit(iter,3) = Dots_PSF_fit(iter,3) - dim.X*PixelSize;
        Dots_PSF_fit(iter,4) = Dots_PSF_fit(iter,4) - dim.Y*PixelSize;
        Dots_PSF_fit(iter,5) = Dots_PSF_fit(iter,5) - dim.Z*zStep;
    elseif PSF_Fit_Flag == 1
        % IF THE PSF IS NOT NECCESARILY SYMMETRIC IN XY AND THERE ARE 8 COLUMNS
        Dots_PSF_fit(iter,4) = Dots_PSF_fit(iter,4) - dim.X*PixelSize;
        Dots_PSF_fit(iter,5) = Dots_PSF_fit(iter,5) - dim.Y*PixelSize;
        Dots_PSF_fit(iter,6) = Dots_PSF_fit(iter,6) - dim.Z*zStep;
    end
    
    % PSF-fitting will precisely locate the dot inside the small cropped 3D
    % image, but you will need to tell it where the small 3D cropped image was
    % taken from in the large imagestack in order to locate it in the large
    % image stack. 
    % Dots_XYZ contains the XYZ coordinates of the center of the small cropped
    % image, so if the small cropped image is say 9 x 9 x 5 pixels, you should
    % use X-corr - 5 and Z-corr - 3. 
    % IF THE PSF IS SYMMETRIC IN XY, THERE ARE 7 COLUMNS
    if PSF_Fit_Flag == 2
        FinalDots(iter, 1:2) = horzcat(Dots_PSF_fit(iter,1), Dots_PSF_fit(iter,1));
        FinalDots(iter, 3) = Dots_PSF_fit(iter,2);
        FinalDots(iter,4) = Dots_PSF_fit(iter,3) + PixelSize * (Dots_XYZ(iter,1) - ceil(dim.X/2));
        FinalDots(iter,5) = Dots_PSF_fit(iter,4) + PixelSize * (Dots_XYZ(iter,2) - ceil(dim.Y/2));
        FinalDots(iter,6) = Dots_PSF_fit(iter,5) + zStep * (Dots_XYZ(iter,3) - ceil(dim.Z/2));
        FinalDots(iter, 7:8) = Dots_PSF_fit(iter,6:7);
    elseif PSF_Fit_Flag == 1
        % IF THE PSF IS NOT NECCESARILY SYMMETRIC IN XY, THERE ARE 8 COLUMNS
        FinalDots(iter, 1:3) = Dots_PSF_fit(iter,1:3);
        FinalDots(iter,4) = Dots_PSF_fit(iter,4) + PixelSize * (Dots_XYZ(iter,1) - ceil(dim.X/2));
        FinalDots(iter,5) = Dots_PSF_fit(iter,5) + PixelSize * (Dots_XYZ(iter,2) - ceil(dim.Y/2));
        FinalDots(iter,6) = Dots_PSF_fit(iter,6) + zStep * (Dots_XYZ(iter,3) - ceil(dim.Z/2));
        FinalDots(iter, 7:8) = Dots_PSF_fit(iter,7:8);
    end
    
end 




end

